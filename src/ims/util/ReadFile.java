package ims.util;

import java.nio.file.*;
import java.io.*;

public class ReadFile {

    public ReadFile() {
        
    }
    
    public String[] readInfoFile(String path) {
        
        //create file Path
        Path file = Paths.get(path);
        String dbDetails = "";
        int strIndex = 0;

        try
        {
            //create file input stream
            InputStream input = new BufferedInputStream(Files.newInputStream(file));

            //create reader (similar to Scanner for keyboard input)
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            //read the first line from the file
            dbDetails = reader.readLine();			

            //close the reader
            reader.close();

        } catch(Exception e) {

            System.out.println("Message: " + e);
            
        } finally {

            System.out.println(dbDetails);
            return dbDetails.split(",");
        }
    }


//    public static void main(String[] args) {
//        
//        ReadFile reader = new ReadFile();
//        
//        String[] info = reader.readInfoFile("data/dbdetails.txt");
//        
//        for(int i = 0; i < info.length; i++) {
//            
//            System.out.println(info[i]);
//        }
//            
//    }
}
