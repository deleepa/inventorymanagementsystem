/**
 *
 * @author mijael
 */

package ims.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {

    String url = "";
    String user = "";
    String password = "";
    static final String DRIVER = "com.mysql.jdbc.Driver";
    static final String PATH = "data/dbdetails.txt";
    
    public void setValues() {
        url = "jdbc:mysql://localhost:3306/ims_database";
        user = "root"; 
        password = "";
    }
    
    public Connection getConnection() {
        Connection dbConnection = null;
        
        try {
            // creates a MySQL Driver instance and creates a connection to the 
            // MySQL database at the given proxy address
            setValues();
//            System.out.println("DriverManager.getDriver " + DriverManager.getDriver(url));
            Class.forName(DRIVER).newInstance();
            
            dbConnection = DriverManager.getConnection(url, user, password);
            
        } catch (Exception e) {
            System.out.println("error  ss "  + e.getMessage() );
            e.printStackTrace();
//          System.exit(-1);
        }
        
        return dbConnection;
    }
    
    //closes the connection that was created by getConnection();
    public void closeConnection(Connection dbConnection, String className) {
        try {
            dbConnection.close();
            
        } catch (Exception e1) {
            System.out.println(className
                               + ": Could not close database connection");
            e1.printStackTrace();
        }

    }
    
//    public static void main(String[] args) {
//        ConnectionDB conn = new ConnectionDB();
//        conn.setValues();
//    }
}
