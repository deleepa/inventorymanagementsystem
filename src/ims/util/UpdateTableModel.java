/**
 * Authour: Deleepa
 * Date:    05/06/2013
 * Project: InventoryManagementSystem  |  UpdateTableModel
 */

package ims.util;

import javax.swing.table.AbstractTableModel;

public class UpdateTableModel extends AbstractTableModel  {
    
    private String[] columnNames = {"Asset ID", "User", "Vendor", "CPU"};
    private Object[][] data;
        
    public UpdateTableModel(int[] arrayIds, String[] users, String[] vendors, String[] cpuBrands){
        
        data = new Object[arrayIds.length][4];
        
        for (int i = 0; i < arrayIds.length; i++) {
            System.out.println("arrayIds.length: " + arrayIds.length);
            
            data[i][0] = arrayIds[i];
            data[i][1] = users[i];
            data[i][2] = vendors[i];
            data[i][3] = cpuBrands[i];
        }
        
    }
    
    @Override
    public Object getValueAt(int row, int col){
        return data[row][col];
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return data.length;
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
}
