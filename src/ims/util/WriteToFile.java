/*
 * Application: Hangman (Class: WriteToFile)
 * Authour:     Deleepa
 * Date:        25th Feb 2013
 */
package ims.util;

import java.nio.file.*;
import java.io.*;
import static java.nio.file.StandardOpenOption.*;

public class WriteToFile {
    
    public WriteToFile(String folder, String file, String writeData) {
        String path;
        path = "" + folder + "" + file;
        Path filePath = Paths.get(path);
        
        try {
            //create the folder if not exist
            File checkDir = new File(folder);

            if(!checkDir.exists()) {
                    
                System.out.println("directory doesn't exist. creating now...");
                boolean result = checkDir.mkdirs();
                        
                if(result) {
                            
                    System.out.println("directory created");
                }
            }
            
            // create output stream
            OutputStream output = new BufferedOutputStream(Files.newOutputStream(filePath));

            //create writer (similar to System.out)
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));

            // write each line to the file
            writer.write(writeData);
            
            //close the file
            writer.close();
        }
        catch(Exception e) {
            System.out.println("Error: " + e);
        }
    }
    
//    public static void main (String[] args) {
//        //String [] test = {"test 1", "test 2"};
//        String test = "test writing to multiple folders";
//        WriteToFile write = new WriteToFile("test1/test2/", "test.txt", test);
//    } 
}
