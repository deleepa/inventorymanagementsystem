/*
 * Authour: deleepa
 * Date:    27/02/2013
 * Project: InventoryManagementSystem | MainClass
 */

package ims.main;

import ims.GUIInterface.*;

public class InventoryManagementSystem {

    //main method
    public static void main(String[] args) {
        GUIInsertNew main = new GUIInsertNew();
        main.setVisible(true);
    }

}
