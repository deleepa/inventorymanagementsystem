/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;

/**
 *
 * @author mijael
 */
public class TBody implements Serializable {

    private static final long serialVersionUID = 1L;
    private String serialNo;
    private String desc;
    private Integer bodyId;
    private int hddId;
    private int mboardId;
    private int osId;
    private int pcClassId;

    public TBody() {
    }

    public TBody(Integer bodyId) {
        this.bodyId = bodyId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getBodyId() {
        return bodyId;
    }

    public void setBodyId(Integer bodyId) {
        this.bodyId = bodyId;
    }

    public int getHddId() {
        return hddId;
    }

    public void setHddId(int hddId) {
        this.hddId = hddId;
    }

    public int getMboardId() {
        return mboardId;
    }

    public void setMboardId(int mboardId) {
        this.mboardId = mboardId;
    }

    public int getOsId() {
        return osId;
    }

    public void setOsId(int osId) {
        this.osId = osId;
    }

    public int getPcClassId() {
        return pcClassId;
    }

    public void setPcClassId(int pcClassId) {
        this.pcClassId = pcClassId;
    }

}
