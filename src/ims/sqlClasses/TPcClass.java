/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class TPcClass implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pcClassId;
    private String className;
    private Collection<TBody> tBodyCollection;

    public TPcClass() {
    }

    public TPcClass(Integer pcClassId) {
        this.pcClassId = pcClassId;
    }

    public Integer getPcClassId() {
        return pcClassId;
    }

    public void setPcClassId(Integer pcClassId) {
        this.pcClassId = pcClassId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Collection<TBody> getTBodyCollection() {
        return tBodyCollection;
    }

    public void setTBodyCollection(Collection<TBody> tBodyCollection) {
        this.tBodyCollection = tBodyCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pcClassId != null ? pcClassId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TPcClass)) {
            return false;
        }
        TPcClass other = (TPcClass) object;
        if ((this.pcClassId == null && other.pcClassId != null) || (this.pcClassId != null && !this.pcClassId.equals(other.pcClassId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TPcClass[ pcClassId=" + pcClassId + " ]";
    }
    
}
