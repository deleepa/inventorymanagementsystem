/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author mijael
 */
public class TPurchase implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer purchaseId;
    private String purchaseDate;
    private String invoiceNo;
    private Collection<TAsset> tAssetCollection;
    private int vendorId;

    public TPurchase() {
    }

    public TPurchase(Integer purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Integer getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Integer purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Collection<TAsset> getTAssetCollection() {
        return tAssetCollection;
    }

    public void setTAssetCollection(Collection<TAsset> tAssetCollection) {
        this.tAssetCollection = tAssetCollection;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (purchaseId != null ? purchaseId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TPurchase)) {
            return false;
        }
        TPurchase other = (TPurchase) object;
        if ((this.purchaseId == null && other.purchaseId != null) || (this.purchaseId != null && !this.purchaseId.equals(other.purchaseId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TPurchase[ purchaseId=" + purchaseId + " ]";
    }
    
}
