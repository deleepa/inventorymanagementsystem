/*
 * Authour: deleepa
 * Date:    27/02/2013
 * Project: InventoryManagementSystem | IMSMainClass
 */

package ims.sqlClasses;

public class IMSMainClass {
    
    //t_user
    int userID;
    String firstName;
    String lastName;
    int deptID;
    
    //t_allocation
    int allocationID;
    
    //t_asset
    int assetID;
    
    //t_purchase
    int purchaseID;
    String purchaseDate; //convert into sql date
    String invoiceNumber;
    
    //t_maintainence
    int maintainenceLogID;
    String technicianName;
    String maintainenceLogDetails;
    String maintanenceLogDate;
    
    //t_dept
    int departmentID;
    String departmentName;
    String departmentLocation;
    String departmentDescription;
    
    //t_pcmain
    int pcMainID;
    
    //t_peripheral
    int peripheralID;
    String peripheralType;
    String peripheralSerialNumber;
    String peripheralDescription;
    
    //t_vendor
    int vendorID;
    String vendorName;
    String vendorAddress;
    String vendorContactPerson;
    String vendorContactNumber; //should be number
    String vendorContactNumber2; //should be number
    
    //t_body
    int bodyID;
    String bodySerialNumber;
    String bodyDescription;
    
    //t_motherboard
    int motherboardID;
    String motherboardName;
    String motherboardDescription;
    
    //t_monitor
    int monitorID;
    String monitorSerialNumber;
    String monitorBrandName;
    String monitorSize;
    String monitorType;
    
    //t_pc_class
    int pcClassID;
    String pcClassName;
    
    //t_hdd
    int hddID;
    String hddCapacity;
    String hddType;
    String hddBrand;
    
    //t_os
    int OSID;
    String OSName;
    String OSVersion;
    
    //t_ram
    int ramID;
    String ramCapacity;
    String ramType;
    String ramBrand;
    int ramCount;
    
    //t_cpu
    int cpuID;
    String cpuName;
    String cpuSpeed;
    String cpuDescription;
    
    //t_gpu
    int gpuID;
    String gpuName;
    String gpuRam;
    String gpuType;
    
    //constructor
    public IMSMainClass() {

    }
    
    // setters and getter for t_user
    public void setUserID(int id) {
        userID = id;
    }
    
    public int getUserID(){
        return userID;
    }
    
    public void setfirstName(String name) {
        firstName = name;
    }
    
    public String getfirstName(){
        return firstName;
    }
    
    public void setLastName(String name) {
        lastName = name;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setDeptID(int id) {
        deptID = id;
    }
    public int getDeptID(){
        return deptID;
    }
    
    //setter and getter for t_allocation
    public void setAllocationID(int id) {
        allocationID = id;
    }
    
    public int getAllocationID() {
        return allocationID;
    }
    
    //setter and getter for t_asset
    public void setAssetID(int id) {
        assetID = id;
    }
    
    public int getAssetID() {
        return assetID;
    }
    
    //setter and getter for t_purchase
    public void setPurchaseID(int id) {
        purchaseID = id;
    }
    
    public int getPurchaseID() {
        return purchaseID;
    }
    
    public void setPurchaseDate(String date) {
        //**convert to sql date format
        purchaseDate = date;
    }
    
    public String getPurchaseDate() {
        return purchaseDate;
    }
    
    public void setInvoiceNumber(String invoice) {
        invoiceNumber = invoice;
    }
    
    public String getInvoiceNumber() {
        return invoiceNumber;
    }
    
    //setters and getters for t_maintainence
    public void setMaintainenceLogID(int id) {
        maintainenceLogID = id;
    }
    
    public int getMaintainenceLogID() {
        return maintainenceLogID;
    }
    
    public void setTechnicianName(String name) {
        technicianName = name;
    }
    
    public String getTechnicianName() {
        return technicianName;
    }
    
    public void setMaintainenceLogDetails(String details) {
        maintainenceLogDetails = details;
    }
    
    public String getMaintainenceLogDetails() {
        return maintainenceLogDetails;
    }
    
    public void setMaintanenceLogDate(String date) {
        //**convert date to sql date format
        maintanenceLogDate = date;
    }
    
    public String getMaintanenceLogDate() {
        return maintanenceLogDate;
    }
    
    //setter and getter for t-dept
    public void setDepartmentID(int id) {
        departmentID = id;
    }
    
    public int getDepartmentID() {
        return departmentID;
    }
    
    public void setDepartmentName(String name) {
        departmentName = name;
    }
    
    public String getDepartmentName() {
        return departmentName;
    }
    
    public void setDepartmentLocation(String location) {
        departmentLocation = location;
    }
    
    public String getDepartmentLocation() {
        return departmentLocation;
    }
    
    public void setDepartmentDescription(String description) {
        departmentDescription = description;
    }
    
    public String getDepartmentDescription() {
        return departmentDescription;
    }
    
    //setters and getters for t_pcmain
    public void setPCMainID(int id) {
        pcMainID = id;
    }
    
    public int getPCMainID() {
        return pcMainID;
    }

    //setters and getter for t_peripherals
    public void setPeripheralID(int id) {
        peripheralID = id;
    }
    
    public int getPeripheralID() {
        return peripheralID;
    }
    
    public void setPeripheralType(String type) {
        peripheralType = type;
    }
    
    public String getPeripheralType() {
        return peripheralType;
    }
    
    public void setPeripheralSerialNumber(String serial) {
        peripheralSerialNumber = serial;
    }
    
    public String getPeripheralSerialNumber() {
        return peripheralSerialNumber;
    }
    
    public void setPeripheralDescription(String description) {
        peripheralDescription = description;
    }
    
    public String getPeripheralDescription() {
        return peripheralDescription;
    }
    
    //setter and getter for t_vendor    
    public void setVendorID(int id) {
        vendorID = id;
    }
    
    public int getVendorID() {
        return vendorID;
    }
    
    public void setVendorName(String name) {
        vendorName = name;
    }
    
    public String getVendorName() {
        return vendorName;
    }
    
    public void setVendorAddress(String address) {
        vendorAddress = address;
    }
    
    public String getVendorAddress() {
        return vendorAddress;
    }
    
    public void setVendorContactPerson(String name) {
        vendorContactPerson = name;
    }
    
    public String getVendorContactPerson() {
        return vendorContactPerson;
    }
    
    public void setVendorContactNumber(String number) {
        vendorContactNumber = number;
    }
    
    public String getvendorContactNumber() {
        return vendorContactNumber;
    }
    
    public void setVendorContactNumber2(String number) {
        vendorContactNumber2 = number;
    }
    
    public String getvendorContactNumber2() {
        return vendorContactNumber2;
    }
    
    //setters and getters for t_body
    public void setBodyID(int id) {
        bodyID = id;
    }
    
    public int getBodyID() {
        return bodyID;
    }
    
    public void setBodySerialNumber(String serial) {
        bodySerialNumber = serial;
    }
    
    public String getBodySerialNumber() {
        return bodySerialNumber;
    }

    public void setBodyDescription(String description) {
         bodyDescription = description;
    }
    
    public String getBodyDescription() {
        return bodyDescription;
    }
    
    //setter and getter for t_motherboard
    public void setMotherboardID(int id) {
        motherboardID = id;
    }
    
    public int getMotherboardID() {
        return motherboardID;
    }
    
    public void setMotherboardName(String name) {
        motherboardName = name;
    }
    
    public String getMotherboardName() {
        return motherboardName;
    }
    
    public void setMotherboardDescription(String description) {
         motherboardDescription = description;
    }
    
    public String getMotherboardDescription() {
        return motherboardDescription;
    }
    
    //setters and getters for t-monitor    
    public void setMonitorID(int id) {
        monitorID = id;
    }
    
    public int getMonitorID() {
        return monitorID;
    }
    
    public void setMonitorSerialNumber(String name) {
        monitorSerialNumber = name;
    }
    
    public String getMonitorSerialNumber() {
        return monitorSerialNumber;
    }
    
    public void setMonitorBrandName(String brandName) {
         monitorBrandName = brandName;
    }
    
    public String getMonitorBrandName() {
        return monitorBrandName;
    }
    
    public void setMonitorSize(String size) {
        monitorSize = size;
    }
    
    public String getMonitorSize() {
        return monitorSize;
    }
    
    public void setMonitorType(String type) {
         monitorType = type;
    }
    
    public String getMonitorType() {
        return monitorType;
    }
    
    //setters and getters for t_pc_class  
    public void setPCClassID(int id) {
        pcClassID = id;
    }
    
    public int getPCClassID() {
        return pcClassID;
    }
        
    public void setPCClassName(String name) {
        pcClassName = name;
    }
    
    public String getPCClassName() {
        return pcClassName;
    }
    
    //setters and getters for t_hdd    
    public void setHDDID(int id) {
        hddID = id;
    }
    
    public int getHDDID() {
        return hddID;
    }
    
    public void setHDDCapacity(String capacity) {
        hddCapacity = capacity;
    }
    
    public String getHDDCapacity() {
        return hddCapacity;
    }
    
    public void setHDDType(String type) {
         hddType = type;
    }
    
    public String getHDDType() {
        return hddType;
    }
    
    public void setHDDBrand(String brand) {
        hddBrand = brand;
    }
    
    public String getHDDBrand() {
        return hddBrand;
    }
    
     //setter and getters for t_os
    public void setOSID(int id) {
        OSID = id;
    }
    
    public int getOSID() {
        return OSID;
    }
    
    public void setOSName(String name) {
        OSName = name;
    }
    
    public String getOSName() {
        return OSName;
    }
    
    public void setOSVersion(String version) {
         OSVersion = version;
    }
    
    public String getOSVersion() {
        return OSVersion;
    }

    //setters and getters for t_ram
    public void setRamID(int id) {
        ramID = id;
    }

    public int getRamID() {
        return ramID;
    }

    public void setRamCapacity(String capacity) {
        ramCapacity = capacity;
    }
    
    public String getRamCapacity() {
        return ramCapacity;
    }

    public void setRamType(String type) {
        ramType = type;
    }

    public String getRamType() {
        return ramType;
    }

    public void setRamBrand(String brand) {
        ramBrand = brand;
    }

    public String getRamBrand() {
        return ramBrand;
    }

    public void setRamCount(int count) {
        ramCount = count;
    }

    public int getRamCount() {
        return ramCount;
    }

    //setters and getters for t_cpu
    public void setCpuID(int id) {
        cpuID = id;
    }

    public int getCpuID() {
        return cpuID;
    }

    public void setCpuName(String name) {
        cpuName = name;
    }
    
    public String getCpuName() {
        return cpuName;
    }

    public void setCpuSpeed(String speed) {
        cpuSpeed = speed;
    }

    public String getCpuSpeed() {
        return cpuSpeed;
    }

    public void setCpuDescription(String description) {
        cpuDescription = description;
    }

    public String getCpuDescription() {
        return cpuDescription;
    }
    
    //setters and getters for t_gpu
    public void setGpuID(int id) {
        gpuID = id;
    }

    public int getGpuID() {
        return gpuID;
    }

    public void setGpuName(String name) {
        gpuName = name;
    }
    
    public String getGpuName() {
        return gpuName;
    }

    public void setGpuRam(String ram) {
        gpuRam = ram;
    }

    public String getGpuRam() {
        return gpuRam;
    }

    public void setGpuType(String type) {
        gpuType = type;
    }

    public String getGpuType() {
        return gpuType;
    }
    

}
