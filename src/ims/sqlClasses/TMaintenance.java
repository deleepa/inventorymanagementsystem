/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;


/**
 *
 * @author mijael
 */
public class TMaintenance implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer maintLogId;
    private String technician;
    private String description;
    private String date;
    private Collection<TAsset> tAssetCollection;

    public TMaintenance() {
    }

    public TMaintenance(Integer maintLogId) {
        this.maintLogId = maintLogId;
    }

    public Integer getMaintLogId() {
        return maintLogId;
    }

    public void setMaintLogId(Integer maintLogId) {
        this.maintLogId = maintLogId;
    }

    public String getTechnician() {
        return technician;
    }

    public void setTechnician(String technician) {
        this.technician = technician;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Collection<TAsset> getTAssetCollection() {
        return tAssetCollection;
    }

    public void setTAssetCollection(Collection<TAsset> tAssetCollection) {
        this.tAssetCollection = tAssetCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maintLogId != null ? maintLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TMaintenance)) {
            return false;
        }
        TMaintenance other = (TMaintenance) object;
        if ((this.maintLogId == null && other.maintLogId != null) || (this.maintLogId != null && !this.maintLogId.equals(other.maintLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TMainte[ maintLogId=" + maintLogId + " ]";
    }
    
}
