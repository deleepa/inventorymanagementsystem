/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class THdd implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer hddId;
    private String hddCapacity;
    private String hddBrand;
    private Collection<TBody> tBodyCollection;

    public THdd() {
    }

    public THdd(Integer hddId) {
        this.hddId = hddId;
    }

    public Integer getHddId() {
        return hddId;
    }

    public void setHddId(Integer hddId) {
        this.hddId = hddId;
    }

    public String getHddCapacity() {
        return hddCapacity;
    }

    public void setHddCapacity(String hddCapacity) {
        this.hddCapacity = hddCapacity;
    }

    public String getHddBrand() {
        return hddBrand;
    }

    public void setHddBrand(String hddBrand) {
        this.hddBrand = hddBrand;
    }
    public Collection<TBody> getTBodyCollection() {
        return tBodyCollection;
    }

    public void setTBodyCollection(Collection<TBody> tBodyCollection) {
        this.tBodyCollection = tBodyCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hddId != null ? hddId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof THdd)) {
            return false;
        }
        THdd other = (THdd) object;
        if ((this.hddId == null && other.hddId != null) || (this.hddId != null && !this.hddId.equals(other.hddId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.THdd[ hddId=" + hddId + " ]";
    }
    
}
