/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class TRam implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer ramId;
    private String brand;
    private String ramType;
    private String ramCapacity;
    private String count;
    private Collection<TMboard> tMboardCollection;

    public TRam() {
    }

    public TRam(Integer ramId) {
        this.ramId = ramId;
    }

    public Integer getRamId() {
        return ramId;
    }

    public void setRamId(Integer ramId) {
        this.ramId = ramId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getRamType() {
        return ramType;
    }

    public void setRamType(String ramType) {
        this.ramType = ramType;
    }

    public String getRamCapacity() {
        return ramCapacity;
    }

    public void setRamCapacity(String ramCapacity) {
        this.ramCapacity = ramCapacity;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public Collection<TMboard> getTMboardCollection() {
        return tMboardCollection;
    }

    public void setTMboardCollection(Collection<TMboard> tMboardCollection) {
        this.tMboardCollection = tMboardCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ramId != null ? ramId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TRam)) {
            return false;
        }
        TRam other = (TRam) object;
        if ((this.ramId == null && other.ramId != null) || (this.ramId != null && !this.ramId.equals(other.ramId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TRam[ ramId=" + ramId + " ]";
    }
    
}
