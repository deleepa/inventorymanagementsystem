/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class TBrand implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer brandId;
    private String name;
    private Collection<TModel> tModelCollection;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<TModel> getTModelCollection() {
        return tModelCollection;
    }

    public void setTModelCollection(Collection<TModel> tModelCollection) {
        this.tModelCollection = tModelCollection;
    }

 
}
    
