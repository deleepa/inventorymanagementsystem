/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;

/**
 *
 * @author mijael
 */
public class TAsset implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer assetId;
    private Integer pcmainId;
    private Integer tUserId;
    private Integer vendorId;
    private Integer tMaintenanceId;


    public TAsset() {
    }

    public TAsset(Integer assetId) {
        this.assetId = assetId;
    }

    public Integer getAssetId() {
        return assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }

    public Integer getPcmainId() {
        return pcmainId;
    }

    public void setPcmainId(Integer pcmainId) {
        this.pcmainId = pcmainId;
    }

    public Integer gettUserId() {
        return tUserId;
    }

    public void settUserId(Integer tUserId) {
        this.tUserId = tUserId;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Integer gettMaintenanceId() {
        return tMaintenanceId;
    }

    public void settMaintenanceId(Integer tMaintenanceId) {
        this.tMaintenanceId = tMaintenanceId;
    }

   

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (assetId != null ? assetId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TAsset)) {
            return false;
        }
        TAsset other = (TAsset) object;
        if ((this.assetId == null && other.assetId != null) || (this.assetId != null && !this.assetId.equals(other.assetId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TAsset[ assetId=" + assetId + " ]";
    }
    
}
