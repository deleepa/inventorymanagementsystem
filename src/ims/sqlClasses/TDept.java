/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class TDept implements Serializable {

   private static final long serialVersionUID = 1L;
    private Integer deptId;
    private String deptName;
    private String location;
    private String deptDesc;
    private Collection<TUser> tUserCollection;

    public TDept() {
    }

    public TDept(Integer deptId) {
        this.deptId = deptId;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDeptDesc() {
        return deptDesc;
    }

    public void setDeptDesc(String deptDesc) {
        this.deptDesc = deptDesc;
    }

    public Collection<TUser> getTUserCollection() {
        return tUserCollection;
    }

    public void setTUserCollection(Collection<TUser> tUserCollection) {
        this.tUserCollection = tUserCollection;
    }
    
}
