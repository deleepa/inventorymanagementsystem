/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;

/**
 *
 * @author mijael
 */
public class TPcMain implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer pcmainId;
    private Integer gpuId;
    private Integer hddId;
    private Integer cpuId;
    private Integer osId;
    private Integer ramId;
    private Integer monitorId;
    private Integer periId;
    private Integer pcClassId;
    
    
    public TPcMain() {
    }

    public TPcMain(Integer pcmainId) {
        this.pcmainId = pcmainId;
    }

    public Integer getGpuId() {
        return gpuId;
    }

    public void setGpuId(Integer gpuId) {
        this.gpuId = gpuId;
    }
    
    public Integer getHddId() {
        return hddId;
    }
    
    public void setHddId(Integer hddId) {
        this.hddId = hddId;
    }
    
    public Integer getCpuId() {
        return cpuId;
    }
    
    public void setCpuId(Integer cpuId) {
        this.cpuId = cpuId;
    }
    
    public Integer getOsId() {
        return osId;
    }
    
    public void setOsId(Integer osId) {
        this.osId = osId;
    }

    public Integer getRamId() {
        return ramId;
    }
    
    public void setRamId(Integer ramId) {
        this.ramId = ramId;
    }
    
    public Integer getMonitorId() {
        return monitorId;
    }
    
    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }
    
    public Integer getPeriId() {
        return periId;
    }
    
    public void setPeriId(Integer periId) {
        this.periId = periId;
    }
    
    public Integer getPcClassId() {
        return pcClassId;
    }
    
    public void setPcClassId(Integer pcClassId) {
        this.pcClassId = pcClassId;
    }
    
    public Integer getPcMainId() {
        return pcmainId;
    }

    public void setPcMainId(Integer pcmainId) {
        this.pcmainId = pcmainId;
    }
}
