/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class TPeripherals implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer periId;
    private String type;
    private String perSerialNo;
    private String desc;
    private Collection<TPcMain> tPcmainCollection;

    public TPeripherals() {
    }

    public TPeripherals(Integer periId) {
        this.periId = periId;
    }

    public Integer getPeriId() {
        return periId;
    }

    public void setPeriId(Integer periId) {
        this.periId = periId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPerSerialNo() {
        return perSerialNo;
    }

    public void setPerSerialNo(String perSerialNo) {
        this.perSerialNo = perSerialNo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Collection<TPcMain> getTPcmainCollection() {
        return tPcmainCollection;
    }

    public void setTPcmainCollection(Collection<TPcMain> tPcmainCollection) {
        this.tPcmainCollection = tPcmainCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (periId != null ? periId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TPeripherals)) {
            return false;
        }
        TPeripherals other = (TPeripherals) object;
        if ((this.periId == null && other.periId != null) || (this.periId != null && !this.periId.equals(other.periId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TPeripherals[ periId=" + periId + " ]";
    }
    
}
