/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;
/**
 *
 * @author mijael
 */
public class TMboard implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer mboardId;
    private String mboardName;
    private String description;
    private Collection<TBody> tBodyCollection;
    private int ramId;
    private int gpuId;
    private int cpuId;

    public TMboard() {
    }

    public TMboard(Integer mboardId) {
        this.mboardId = mboardId;
    }

    public Integer getMboardId() {
        return mboardId;
    }

    public void setMboardId(Integer mboardId) {
        this.mboardId = mboardId;
    }

    public String getMboardName() {
        return mboardName;
    }

    public void setMboardName(String mboardName) {
        this.mboardName = mboardName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<TBody> getTBodyCollection() {
        return tBodyCollection;
    }

    public void setTBodyCollection(Collection<TBody> tBodyCollection) {
        this.tBodyCollection = tBodyCollection;
    }

    public int getRamId() {
        return ramId;
    }

    public void setRamId(int ramId) {
        this.ramId = ramId;
    }

    public int getGpuId() {
        return gpuId;
    }

    public void setGpuId(int gpuId) {
        this.gpuId = gpuId;
    }

    public int getCpuId() {
        return cpuId;
    }

    public void setCpuId(int cpuId) {
        this.cpuId = cpuId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mboardId != null ? mboardId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TMboard)) {
            return false;
        }
        TMboard other = (TMboard) object;
        if ((this.mboardId == null && other.mboardId != null) || (this.mboardId != null && !this.mboardId.equals(other.mboardId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TMboard[ mboardId=" + mboardId + " ]";
    }
    
}
