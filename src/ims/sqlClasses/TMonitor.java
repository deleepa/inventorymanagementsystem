/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class TMonitor implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer monitorId;
    private String monitorSerialNo;
    private String monitorBrand;
    private String monitorSize;
    
    private Collection<TPcMain> tPcmainCollection;

    public TMonitor() {
    }

    public TMonitor(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public String getMonSerialNo() {
        return monitorSerialNo;
    }

    public void setMonSerialNo(String monitorSerialNo) {
        this.monitorSerialNo = monitorSerialNo;
    }

    public String getBrandName() {
        return monitorBrand;
    }

    public void setBrandName(String monitorBrand) {
        this.monitorBrand = monitorBrand;
    }

    public String getSize() {
        return monitorSize;
    }

    public void setSize(String monitorSize) {
        this.monitorSize = monitorSize;
    }

    public Collection<TPcMain> getTPcmainCollection() {
        return tPcmainCollection;
    }

    public void setTPcmainCollection(Collection<TPcMain> tPcmainCollection) {
        this.tPcmainCollection = tPcmainCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (monitorId != null ? monitorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TMonitor)) {
            return false;
        }
        TMonitor other = (TMonitor) object;
        if ((this.monitorId == null && other.monitorId != null) || (this.monitorId != null && !this.monitorId.equals(other.monitorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TMonitor[ monitorId=" + monitorId + " ]";
    }
    
}
