/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;
/**
 *
 * @author mijael
 */
public class TOs implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer osId;
    private String osName;
    private String osVersion;
    private Collection<TBody> tBodyCollection;

    public TOs() {
    }

    public TOs(Integer osId) {
        this.osId = osId;
    }

    public Integer getOsId() {
        return osId;
    }

    public void setOsId(Integer osId) {
        this.osId = osId;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public Collection<TBody> getTBodyCollection() {
        return tBodyCollection;
    }

    public void setTBodyCollection(Collection<TBody> tBodyCollection) {
        this.tBodyCollection = tBodyCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (osId != null ? osId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TOs)) {
            return false;
        }
        TOs other = (TOs) object;
        if ((this.osId == null && other.osId != null) || (this.osId != null && !this.osId.equals(other.osId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TOs[ osId=" + osId + " ]";
    }
    
}
