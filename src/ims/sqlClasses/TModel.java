/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;
/**
 *
 * @author mijael
 */
public class TModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer modelId;
    private String name;
    private Collection<TAsset> tAssetCollection;
    private TBrand brandBrandId;

    public TModel() {
    }

    public TModel(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<TAsset> getTAssetCollection() {
        return tAssetCollection;
    }

    public void setTAssetCollection(Collection<TAsset> tAssetCollection) {
        this.tAssetCollection = tAssetCollection;
    }

    public TBrand getBrandBrandId() {
        return brandBrandId;
    }

    public void setBrandBrandId(TBrand brandBrandId) {
        this.brandBrandId = brandBrandId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modelId != null ? modelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TModel)) {
            return false;
        }
        TModel other = (TModel) object;
        if ((this.modelId == null && other.modelId != null) || (this.modelId != null && !this.modelId.equals(other.modelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TModel[ modelId=" + modelId + " ]";
    }
    
}
