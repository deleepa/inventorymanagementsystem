/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */

public class TGpu implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer gpuId;
    private String gpuBrand;
    private String gpuRam;
    private String gpuType;
    private Collection<TMboard> tMboardCollection;

    public TGpu() {
    }

    public TGpu(Integer gpuId) {
        this.gpuId = gpuId;
    }

    public Integer getGpuId() {
        return gpuId;
    }

    public void setGpuId(Integer gpuId) {
        this.gpuId = gpuId;
    }

    public String getGpuBrand() {
        return gpuBrand;
    }

    public void setGpuBrand(String gpuBrand) {
        this.gpuBrand = gpuBrand;
    }



    public String getGpuRam() {
        return gpuRam;
    }

    public void setGpuRam(String gpuRam) {
        this.gpuRam = gpuRam;
    }

    public String getGpuType() {
        return gpuType;
    }

    public void setGpuType(String gpuType) {
        this.gpuType = gpuType;
    }

    public Collection<TMboard> getTMboardCollection() {
        return tMboardCollection;
    }

    public void setTMboardCollection(Collection<TMboard> tMboardCollection) {
        this.tMboardCollection = tMboardCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gpuId != null ? gpuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TGpu)) {
            return false;
        }
        TGpu other = (TGpu) object;
        if ((this.gpuId == null && other.gpuId != null) || (this.gpuId != null && !this.gpuId.equals(other.gpuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TGpu[ gpuId=" + gpuId + " ]";
    }
    
}
