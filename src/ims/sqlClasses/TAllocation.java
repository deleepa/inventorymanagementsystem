/**
 *
 * @author mijael
 */
package ims.sqlClasses;

public class TAllocation {

    private Integer allocationId;
    private Integer userId;


    public TAllocation() {
    }

    public TAllocation(Integer allocationId) {
        this.allocationId = allocationId;
    }

    public Integer getAllocationId() {
        return allocationId;
    }

    public void setAllocationId(Integer allocationId) {
        this.allocationId = allocationId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "dip.project2.TAllocation[ allocationId = " + allocationId + " ]";
    }
    
}