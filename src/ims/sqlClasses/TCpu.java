/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mijael
 */
public class TCpu implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private Integer cpuId;
    private String cpuBrand;
    private String speed;
    private Collection<TMboard> tMboardCollection;

    public TCpu() {
    }

    public TCpu(Integer cpuId) {
        this.cpuId = cpuId;
    }

    public Integer getCpuId() {
        return cpuId;
    }

    public void setCpuId(Integer cpuId) {
        this.cpuId = cpuId;
    }

    public String getCpuBrand() {
        return cpuBrand;
    }

    public void setCpuBrand(String cpuBrand) {
        this.cpuBrand = cpuBrand;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }



    @XmlTransient
    public Collection<TMboard> getTMboardCollection() {
        return tMboardCollection;
    }

    public void setTMboardCollection(Collection<TMboard> tMboardCollection) {
        this.tMboardCollection = tMboardCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cpuId != null ? cpuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TCpu)) {
            return false;
        }
        TCpu other = (TCpu) object;
        if ((this.cpuId == null && other.cpuId != null) || (this.cpuId != null && !this.cpuId.equals(other.cpuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TCpu[ cpuId=" + cpuId + " ]";
    }
    
}
