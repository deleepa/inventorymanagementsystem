/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.sqlClasses;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author mijael
 */
public class TUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer userId;
    private String userName;
    private String userStaffId;
    private Collection<TAllocation> tAllocationCollection;

    public TUser() {
    }

    public TUser(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getStaffId() {
        return userStaffId;
    }

    public void setStaffId(String userStaffId) {
        this.userStaffId = userStaffId;
    }

    public Collection<TAllocation> getTAllocationCollection() {
        return tAllocationCollection;
    }

    public void setTAllocationCollection(Collection<TAllocation> tAllocationCollection) {
        this.tAllocationCollection = tAllocationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TUser)) {
            return false;
        }
        TUser other = (TUser) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dip.project2.TUser[ userId=" + userId + " ]";
    }
    
}
