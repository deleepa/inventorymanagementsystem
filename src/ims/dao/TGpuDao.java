/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TGpuDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "gpu";
    String id = "gpu_id";
    String brand = "gpu_brand";
    String ram = "gpu_ram";
    String type = "gpu_type";
        
    //method to insert information into the given table
    public int insertTGpu(TGpu tgpu){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            brand + ", " + 
                            ram + ", " +
                            type + ") values ('" + 
                            tgpu.getGpuBrand()+ "' , '" + 
                            tgpu.getGpuRam()+ "' , '" + 
                            tgpu.getGpuType()+ "'); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
//      ResultSet rs = new re
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.println("Problems in " + TGpuDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TGpuDao.class.toString());
            
        }
    }    
    
    public List<TGpu> selecAllGpu(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TGpu> listTGpu = new ArrayList<TGpu>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TGpu tGpu =  new TGpu();
                tGpu.setGpuId(rs.getInt(id));
                tGpu.setGpuBrand(rs.getString(brand));
                tGpu.setGpuRam(rs.getString(ram));
                tGpu.setGpuRam(rs.getString(type));
               // tGpu.setGpuId(rs.getString("tGpu_id"));
                listTGpu.add(tGpu);
            }
            return listTGpu;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TGpuDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TGpuDao.class.toString());
            
        }
    }
    
    public TGpu selectTGpu(TGpu tGpu){
        
        String selectRow = "SELECT * FROM " + tableName + "WHERE " + id + "= " 
                + tGpu.getGpuId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tGpu.setGpuId(rs.getInt(id));
            tGpu.setGpuBrand(rs.getString(brand));
            tGpu.setGpuRam(rs.getString(ram));
            tGpu.setGpuRam(rs.getString(type));
            return tGpu;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TGpuDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TGpuDao.class.toString());
            
        }
    }
    
        public TGpu selectTGpuFromId(int tGpuId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tGpuId +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TGpu tGpu = new TGpu();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tGpu.setGpuId(rs.getInt(id));
            tGpu.setGpuBrand(rs.getString(brand));
            tGpu.setGpuRam(rs.getString(ram));
            tGpu.setGpuRam(rs.getString(type));
            return tGpu;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TGpuDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TGpuDao.class.toString());
            
        }
    }
    
    public boolean deleteBrand(TGpu tGpuDelete){
        
        String deleteSql = "DELETE FROM " + tableName + "WHERE " + id + " = " 
                + tGpuDelete.getGpuId() + ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TGpuDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TGpuDao.class.toString());
            
        }
    }
    
    public boolean udateTGpu(TGpu tGpuUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " 
                + brand + " = '" + tGpuUpdate.getGpuBrand()+ "' WHERE " + id + " = " 
                + tGpuUpdate.getGpuId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TGpuDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TGpuDao.class.toString());
            
        }
    }
}
          