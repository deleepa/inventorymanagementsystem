/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TDeptDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TDeptDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "t_dept";
    String id = "dept_id";
    String name = "dept_name";
    String location = "location";
    String description = "dept_desc";
        
    //method to insert information into the given table
    public int insertTDept(TDept tdept){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" + 
                            name + ", " +
                            location + ", " +
                            description +
                            ") values ('" + 
                            tdept.getDeptName() + "' , '" +
                            tdept.getLocation()+ "' , '" +
                            tdept.getDeptDesc()+ "' ); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TDeptDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TDeptDao.class.toString());
            
        }
    }    
    
    public List<TDept> selectAllTDept(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TDept> listTDept = new ArrayList<TDept>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TDept tDept =  new TDept();
                tDept.setDeptId(rs.getInt("tDept_id"));
                tDept.setDeptName(rs.getString("tDept_name"));
                tDept.setLocation(rs.getString("tDept_loc"));
                tDept.setDeptDesc(rs.getString("tDept_desc"));
                listTDept.add(tDept);
            }
            return listTDept;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TDeptDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TDeptDao.class.toString());
            
        }
    }
    
    public TDept selectTDept(TDept tDept){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE" + id + " = " 
                + tDept.getDeptId()+";"; 
        Connection c = connectionDB.getConnection();
        
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tDept.setDeptId(rs.getInt("tDept_id"));
            tDept.setDeptName(rs.getString("tDept_name"));
            tDept.setLocation(rs.getString("tDept_loc"));
            tDept.setDeptDesc(rs.getString("tDept_desc"));
            return tDept;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TDeptDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TDeptDao.class.toString());
            
        }
    }
    
    
    public boolean deleteTDept(TDept tDeptDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + id  + "= " 
                + tDeptDelete.getDeptId() +";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TDeptDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TDeptDao.class.toString());
            
        }
    }
    
    public boolean updateTDept(TDept tDeptUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET "
                + name + " = " + tDeptUpdate.getDeptName()  + ", "
                + location + " = " + tDeptUpdate.getLocation()   + ", "
                + description  + " = " + tDeptUpdate.getDeptDesc() + " "
                + " WHERE " + id + " = "  + tDeptUpdate.getDeptId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql); 
        } catch (SQLException ex) {
            System.err.print("Problems in " + TDeptDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TDeptDao.class.toString());
        }
    }
}
          