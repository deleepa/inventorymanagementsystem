/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.dao;
import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

/**
 *
 * @author nick
 */
public class TAllocationDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    

    String tableName = "t_allocation";
    String id = "allocation_id";
    String userId = "user_id";

        
    //method to insert information into the given table
    public int insertTAllocation(TAllocation tAllocation){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            userId +
                            ") values ('" + 

                            tAllocation.getUserId()+ "' ); ";

        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();

            if(! st.execute(sqlInsert) ){

                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAllocationDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TAllocationDao.class.toString());   
        }
    }    
    
   public List<TAllocation> selectAllTAllocation(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TAllocation> listTAllocation;
        listTAllocation = new ArrayList<>();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TAllocation tAllocation =  new TAllocation();
                tAllocation.setUserId(rs.getInt("user_id"));
                listTAllocation.add(tAllocation);
            }
            return listTAllocation;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAllocationDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TAllocationDao.class.toString());
        }
    }
    
    public TAllocation selectTAllocation(TAllocation tAllocation){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tAllocation.getAllocationId()+ ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next(); 
            tAllocation.setUserId(rs.getInt("user_id"));
            return tAllocation;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAllocationDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TAllocationDao.class.toString()); 
        }
    }
    
    public boolean deleteTAllocation(TAllocation tAllocationDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + id + " = "
                + tAllocationDelete.getAllocationId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAllocationDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TAllocationDao.class.toString());
        }
    }
    public boolean updateTAllocation(TAllocation tAllocationUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET "
                + userId + " = " + tAllocationUpdate.getUserId()
                + " WHERE " + id + " = "  + tAllocationUpdate.getAllocationId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAllocationDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TAllocationDao.class.toString());   
        }
    }
    
}
