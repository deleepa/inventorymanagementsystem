/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TRamDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "ram";
    String id = "ram_id";
    String type = "ram_type";
    String brand = "ram_brand";
    String capacity = "ram_capacity" ;
    String count = "ram_count";
        
    //method to insert information into the given table
    public int insertRam(TRam tram){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            type + ", " + 
                            brand + ", " +
                            capacity + ", " +
                            count + ") values ('" + 
                            tram.getRamType()+ "' , '" + 
                            tram.getBrand()+ "' , '" + 
                            tram.getRamCapacity()+ "' , '" + 
                            tram.getCount()+ "'); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TRamDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TRamDao.class.toString());
            
        }
    }    
    
    public List<TRam> selectAllRam(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TRam> listRams = new ArrayList<TRam>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TRam ram =  new TRam();
                ram.setRamId(rs.getInt(id));
                ram.setRamType(rs.getString(type));
                ram.setBrand(rs.getString(brand));
                ram.setRamCapacity(rs.getString(capacity));
                ram.setCount(rs.getString(count));
                listRams.add(ram);
            }
            return listRams;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TRamDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TRamDao.class.toString());
            
        }
    }
    
    public TRam selectRam(TRam ram){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + ram.getRamId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            ram.setRamId(rs.getInt(id));
            ram.setRamType(rs.getString(type));
            ram.setBrand(rs.getString(brand));
            ram.setRamCapacity(rs.getString(capacity));
            ram.setCount(rs.getString(count));
            return ram;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TRamDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TRamDao.class.toString());
            
        }
    }
    
    public TRam selectRamFromId(int ramId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + ramId +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TRam ram = new TRam();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            ram.setRamId(rs.getInt(id));
            ram.setRamType(rs.getString(type));
            ram.setBrand(rs.getString(brand));
            ram.setRamCapacity(rs.getString(capacity));
            ram.setCount(rs.getString(count));
            return ram;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TRamDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TRamDao.class.toString());
            
        }
    }
    
    public boolean deleteRam(TRam ramDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE" + id +" = "  
                + ramDelete.getRamId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TRamDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TRamDao.class.toString());      
        }
    }
    
    public boolean updateRam(TRam ramUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " + 
                        type + " = '" + ramUpdate.getRamType() +"', "+
                        brand + " = '" + ramUpdate.getBrand() +"', "+
                        capacity + " = '" + ramUpdate.getRamCapacity()+"', "+
                        count + " = '" + ramUpdate.getCount() +"' "+
                           " WHERE " + id + " = " + ramUpdate.getRamId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);           
        } catch (SQLException ex) {
            System.err.print("Problems in " + TRamDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TRamDao.class.toString());   
        }
    }
}