/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TMonitorDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();

    String tableName = "monitor";
    String id = "monitor_id";
    String serialNumber ="monitor_serial_no";
    String brandName = "monitor_brand";
    String size = "monitor_size";
 
    //method to insert information into the given table
    public int insertMonitor(TMonitor tMonitor){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            serialNumber + ", " + 
                            brandName + ", " +
                            size + ") values ('" + 

                            tMonitor.getMonSerialNo()+ "' , '" + 
                            tMonitor.getBrandName()+ "' , '" + 
                            tMonitor.getSize()+ "');";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMonitorDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TMonitorDao.class.toString());
            
        }
    }    
    
    public List<TMonitor> selectAllTMonitor(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TMonitor> listTMonitor = new ArrayList<>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);

            while(rs.next()){
                TMonitor tMonitor =  new TMonitor();
                tMonitor.setMonitorId(rs.getInt(id));
                tMonitor.setBrandName(rs.getString(brandName));
                tMonitor.setSize(rs.getString(size));
                tMonitor.setMonSerialNo(serialNumber);
                listTMonitor.add(tMonitor);
            }
            return listTMonitor;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMonitorDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TMonitorDao.class.toString());
            
        }
    }
    
    public TMonitor selectTMonitor(TMonitor tMonitor){
        
      String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tMonitor.getMonitorId()+ ";"; 
       
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tMonitor.setMonitorId(rs.getInt(id));
            tMonitor.setBrandName(rs.getString(brandName));
            tMonitor.setSize(rs.getString(size));
            tMonitor.setMonSerialNo(serialNumber);

            return tMonitor;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMonitorDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TMonitorDao.class.toString());
            
        }
    }
    
    public TMonitor selectTMonitorFromId(int tMonitorId){
        
      String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tMonitorId + ";"; 
       
        Connection c = connectionDB.getConnection();
        
        try {
            TMonitor tMonitor = new TMonitor();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tMonitor.setMonitorId(rs.getInt(id));
            tMonitor.setBrandName(rs.getString(brandName));
            tMonitor.setSize(rs.getString(size));
            tMonitor.setMonSerialNo(serialNumber);

            return tMonitor;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMonitorDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TMonitorDao.class.toString());
            
        }
    }
    
    public boolean deleteTMonitor(TMonitor monitorDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + id + "= " 
                + monitorDelete.getMonitorId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMonitorDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TMonitorDao.class.toString());
            
        }
    }
    
    public boolean updateTMonitor(TMonitor tMonitorUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " 
                + serialNumber + " = '" + tMonitorUpdate.getMonSerialNo()+ "', "
                + brandName + " = '" + tMonitorUpdate.getBrandName()+ "', " 
                + size + " = '" + tMonitorUpdate.getSize()+ "' "
                + " WHERE " + id + " = " + tMonitorUpdate.getMonitorId() +";";
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMonitorDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TMonitorDao.class.toString());
            
        }
    }
}
          
