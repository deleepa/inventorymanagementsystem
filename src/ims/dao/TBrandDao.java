/**
 *
 * @author nic
 */

package ims.dao;

import ims.sqlClasses.*;
import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;

public class TBrandDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "";
    String id = "";
    String name = "";
    
        
    //method to insert information into the given table
    public int insertBrand(TBrand brand){
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            name  + ") values ('" + 
                            brand.getName() + "' , '); ";
        String sqlSelect = "SELECT MAX("+ id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBrandDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TBrandDao.class.toString());
            
        }
    }    
    
    public List<TBrand> selectAllBrand(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TBrand> listBrands = new ArrayList<TBrand>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TBrand brand =  new TBrand();
                brand.setBrandId(rs.getInt("brand_id"));
                brand.setName(rs.getString("brand_name"));
                listBrands.add(brand);
            }
            return listBrands;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBrandDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TBrandDao.class.toString());
            
        }
    }
    public TBrand selectBrand(TBrand brand){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + brand.getBrandId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            brand.setBrandId(rs.getInt(""));
            brand.setName(rs.getString(""));
            return brand;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBrandDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TBrandDao.class.toString());
            
        }
    }
    
    public boolean deleteBrand(TBrand brandDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE" + id +" = "  
                + brandDelete.getBrandId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBrandDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TBrandDao.class.toString());      
        }
    }
    
    public boolean updateBrand(TBrand brandUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " + name + " = " + brandUpdate.getName() + " WHERE" + id + " = " 
                + brandUpdate.getBrandId() +";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);           
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBrandDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TBrandDao.class.toString());   
        }
    }
}
          
