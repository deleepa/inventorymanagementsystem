/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.sqlClasses.TBrand;
import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TModelDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "";
    String id = "";
    String name = "";
        
    //method to insert information into the given table
    public int insertModel(TModel tmodel){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            name + ",) values ('" + 
                            tmodel.getName()+ "');";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TModelDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TModelDao.class.toString());
            
        }
    }    
    
    public List<TModel> selectAllTModel(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TModel> listTModel = new ArrayList<>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TModel tModel =  new TModel();
                tModel.setModelId(rs.getInt("model_id"));
                tModel.setName(rs.getString("name"));
                listTModel.add(tModel);
            }
            return listTModel;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TModelDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TModelDao.class.toString());
            
        }
    }
    
    public TModel selectTModel(TModel tModel){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= "
                + tModel.getModelId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tModel.setModelId(rs.getInt(""));
            tModel.setName(rs.getString(""));
            return tModel;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TModelDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TModelDao.class.toString());
            
        }
    }
    
    public boolean deleteTModel(TModel modelDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + id + " =" 
                + modelDelete.getModelId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TModelDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TModelDao.class.toString());
            
        }
    }
    
    public boolean updateTModel(TModel tModelUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " 
                + name + " = " + tModelUpdate.getName() + ", "
                + id + " = " + tModelUpdate.getModelId() + ", "
                + " WHERE " + id + " =" + tModelUpdate.getModelId() +";";
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TModelDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TModelDao.class.toString());
            
        }
    }
}
          