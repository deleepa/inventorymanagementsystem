/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.dao;

import ims.sqlClasses.TPcMain;
import ims.util.ConnectionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author mijael
 */
public class TPcMainDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    String tableName="pc_main";
    String id ="pc_main_id";
    String gpuId ="gpu_id";
    String hddId ="hdd_id";
    String cpuId ="cpu_id";
    String osId ="os_id";
    String ramId ="ram_id";
    String monitorId= "monitor_id";
    String peripheralId= "peripheral_id";
    String pcClassId= "pc_class_id";
    
    //method to insert information into the given table
    public int insertPcMain(TPcMain pcMain){
        
        String sqlInsert = "INSERT INTO " + tableName + " ( " +
                            gpuId + ", " +
                            hddId + ", " +
                            cpuId + ", " +
                            osId + ", " +
                            ramId + ", " +
                            monitorId + ", " +
                            peripheralId + ", " +
                            pcClassId + ") values ('" + 
                            
                            pcMain.getGpuId()+ "' , '" +
                            pcMain.getHddId()+ "' , '" +
                            pcMain.getCpuId()+ "' , '" +
                            pcMain.getOsId()+ "' , '" +
                            pcMain.getRamId()+ "' , '" +
                            pcMain.getMonitorId()+ "' , '" +
                            pcMain.getPeriId()+ "' , '" +
                            pcMain.getPcClassId()+ "'); ";
        String sqlSelect = "SELECT MAX(" + id +") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcMainDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TPcMainDao.class.toString());
            
        }
    }
    
    public TPcMain selectTPcMainFromId(int tPcMainId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tPcMainId+ ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TPcMain tPcMain = new TPcMain();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next(); 
            tPcMain.setPcMainId(rs.getInt(id));
            tPcMain.setGpuId(rs.getInt(gpuId));
            tPcMain.setHddId(rs.getInt(hddId));
            tPcMain.setCpuId(rs.getInt(cpuId));
            tPcMain.setOsId(rs.getInt(osId));
            tPcMain.setRamId(rs.getInt(ramId));
            tPcMain.setMonitorId(rs.getInt(monitorId));
            tPcMain.setPeriId(rs.getInt(peripheralId));
            tPcMain.setPcClassId(rs.getInt(pcClassId));
            return tPcMain;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcMainDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TPcMainDao.class.toString()); 
        }
    }
}
