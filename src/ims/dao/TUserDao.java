/**
 * Author: Mijael
 * Date:    17/03/2013
 * Project: TUserDao
 * Description: Managment Data access object from Tuser
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import ims.sqlClasses.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TUserDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "user";
    String id = "user_id";
    String userName = "user_name";
    String staffId = "user_staff_id";
        
    //method to insert information into the given table
    public int insertUser(TUser tuser){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            userName + ", " + 
                            staffId + ") values ('" + 
                
                            tuser.getUserName()+ "' , '" +
                            tuser.getStaffId()+ "');";
        
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TUserDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TUserDao.class.toString());
            
        }
    }    

    public List<TUser> selectAllTUser(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TUser> listTUser = new ArrayList<TUser>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TUser tUser =  new TUser();
                tUser.setUserId(rs.getInt(id));
                tUser.setUserName(rs.getString(userName));
                tUser.setStaffId(rs.getString(id));
                listTUser.add(tUser);
            }
            return listTUser;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TUserDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TUserDao.class.toString());
            
        }
    }
    
    public TUser selectTUser(TUser tUser){     
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + tUser.getUserId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tUser.setUserId(rs.getInt(id));
            tUser.setUserName(rs.getString(userName));
            tUser.setStaffId(rs.getString(id));
            return tUser;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TUserDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TUserDao.class.toString());          
        }
    }
    
        public TUser selectTUserFromId(int tUserId){     
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + tUserId+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TUser tUser = new TUser();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tUser.setUserId(rs.getInt(id));
            tUser.setUserName(rs.getString(userName));
            tUser.setStaffId(rs.getString(id));
            return tUser;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TUserDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TUserDao.class.toString());          
        }
    }
    
    /*
     * When searching by name, the chances of returning multiple
     * records are higher. Therefore if there are multiple records
     * the method must return all the records as TUser objects in a HashMap. 
     */
    public HashMap<Integer, TUser> selectTUserByName(String userNameQuery){     
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + userName +" = '" 
                + userNameQuery +"';"; 
        System.out.println("query: " + selectRow);
        Connection c = connectionDB.getConnection();
        
        try {
            //create the connection and get the results
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.last();
            
            /*
             * move the results set pointer to the last record to get
             * the total number of records. Use that to declare an array 
             * of TUser objects to put into the hash map.
             * The hashmap will be populated by TUser objects and the respective
             * integers
             */
            int index = 0;
            boolean next;
            System.out.println("rs.getRow(): " + rs.getRow());
            TUser[] tUser = new TUser[rs.getRow()];
            HashMap<Integer, TUser> tUserMap = new HashMap<Integer, TUser>();
            rs.first();
            
            do {
                tUser[index] = new TUser();
                tUser[index].setUserId(rs.getInt(id));
                tUser[index].setUserName(rs.getString(userName));
                tUser[index].setStaffId(rs.getString(id));
                tUserMap.put(index, tUser[index]);
                System.out.println("index: " + index);
                
                next = rs.next();
                index++;
                
            } while(next);                        
            return tUserMap;
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TUserDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TUserDao.class.toString());          
        }
    } 

    public boolean deleteUser(TUser user){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE "+id+" = '" 
                + user.getUserId() +"';"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return !st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TUserDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TUserDao.class.toString());
            
        }
    }
    
    public boolean udateUser(TUser user){
        
        String updateSql = "UPDATE " + tableName + " SET " +   
                userName + " =  '"+ user.getUserName()+"', " + 
                staffId + " =  '"+ user.getStaffId() +"' "+ 
                " WHERE " + id + " = " + user.getUserId()  +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TUserDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TUserDao.class.toString());
            
        }
    }
}
          