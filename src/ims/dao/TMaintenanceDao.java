/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.sqlClasses.TBrand;
import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TMaintenanceDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();

      String tableName = "maintenance";
      String logId = "maintenance_id";
      String technician = "maintenance_technician";
      String description = "maintenance_description";
      String date = "maintenance_date";

        
    //method to insert information into the given table
    public int insertMaintenance(TMaintenance tmaintenance){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            technician + ", " + 
                            description + ", " +
                            date + ")  values ('" + 
                            tmaintenance.getTechnician()+ "' , '" + 
                            tmaintenance.getDescription()+ "' , '" + 
                            tmaintenance.getDate()+ "'); ";
        String sqlSelect = "SELECT MAX(" + logId + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMaintenanceDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TMaintenanceDao.class.toString());
            
        }
    }    
    
    public List<TMaintenance> selectAllTMaintenance(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TMaintenance> listTMaintenance = new ArrayList<TMaintenance>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TMaintenance tMaintenance =  new TMaintenance();
                tMaintenance.setMaintLogId(rs.getInt("maint_log_logId"));
                tMaintenance.setTechnician(rs.getString("maint_log_techName"));
                tMaintenance.setDescription(rs.getString("maint_log_desc"));
                tMaintenance.setDate(rs.getString("maint_log_date"));
                listTMaintenance.add(tMaintenance);
            }
            return listTMaintenance;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMaintenanceDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TMaintenanceDao.class.toString());
            
        }
    }
    
    public TMaintenance selectTMaintenance(TMaintenance tMaintenance){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + logId + " = " 
                + tMaintenance.getMaintLogId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tMaintenance.setMaintLogId(rs.getInt("maint_log_logId"));
            tMaintenance.setTechnician(rs.getString("maint_log_techName"));
            tMaintenance.setDescription(rs.getString("maint_log_desc"));
            tMaintenance.setDate(rs.getString("maint_log_date"));
            return tMaintenance;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMaintenanceDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TMaintenanceDao.class.toString());
            
        }
    }
    
        public TMaintenance selectTMaintenanceFromId(int maintenanceId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + logId + " = " 
                + maintenanceId +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TMaintenance tMaintenance = new TMaintenance();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tMaintenance.setMaintLogId(rs.getInt(logId));
            tMaintenance.setTechnician(rs.getString(technician));
            tMaintenance.setDescription(rs.getString(description));
            tMaintenance.setDate(rs.getString(date));
            return tMaintenance;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMaintenanceDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TMaintenanceDao.class.toString());
            
        }
    }
    
    public boolean deleteMaintenance(TMaintenance maintenanceDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + logId + " = " 
                + maintenanceDelete.getMaintLogId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMaintenanceDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TMaintenanceDao.class.toString());
            
        }
    }
    
    public boolean udateTMaintenance(TMaintenance tMaintenanceUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " 
                + technician + " = '" + tMaintenanceUpdate.getTechnician()+ "', "
                + description  + " = '"+ tMaintenanceUpdate.getDescription()+ "', "
                + date + " = '" + tMaintenanceUpdate.getDate()
                + "' WHERE " + logId + " = "  + tMaintenanceUpdate.getMaintLogId() + ";";  
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMaintenanceDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TMaintenanceDao.class.toString());
            
        }
    }
}
          