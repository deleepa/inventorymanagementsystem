/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TPurchaseDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "purchase";
    String id = "purchase_id";
    String date = "purchase_date";
    String invoiceNumber = "invoice_no";
        
    //method to insert information into the given table
    public int insertMaintenance(TPurchase tpurchase){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            date + ", " + 
                            invoiceNumber + ") values ('" + 
                            tpurchase.getPurchaseDate() + "' , '" +
                            tpurchase.getInvoiceNo() + "'); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPurchaseDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TPurchaseDao.class.toString());
            
        }
    }    
    
    public List<TPurchase> selectAllPurchase(){
        
        String sqlSelectAll = "SELECT * FROM " + tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TPurchase> listPurchases = new ArrayList<TPurchase>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TPurchase purchase =  new TPurchase();
                purchase.setPurchaseId(rs.getInt(""));
                purchase.setPurchaseDate(rs.getString(""));
                purchase.setInvoiceNo(rs.getString(""));
                listPurchases.add(purchase);
            }
            return listPurchases;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPurchaseDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TPurchaseDao.class.toString());
            
        }
    }
    
    public TPurchase selectPurchase(TPurchase purchase){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + purchase.getPurchaseId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            purchase.setPurchaseId(rs.getInt(""));
            purchase.setPurchaseDate(rs.getString(""));
            purchase.setInvoiceNo(rs.getString(""));
            return purchase;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPurchaseDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TPurchaseDao.class.toString());
            
        }
    }
    
    public boolean deletePurchase(TPurchase purchaseDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE" + id +" = "  
                + purchaseDelete.getPurchaseId() + ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPurchaseDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TPurchaseDao.class.toString());      
        }
    }
    
   public boolean updatePurchase(TPurchase purchaseUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " + id + " = " + purchaseUpdate.getPurchaseId() + 
                                                             date + " = " + purchaseUpdate.getPurchaseDate() +
                                                             invoiceNumber + " = " + purchaseUpdate.getInvoiceNo() +
                           " WHERE" + id + " = " + purchaseUpdate.getPurchaseId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);           
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPurchaseDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TPurchaseDao.class.toString());   
        }
    }
}