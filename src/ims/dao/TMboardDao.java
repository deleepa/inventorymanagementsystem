/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.sqlClasses.TBrand;
import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TMboardDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "t_mboard";
    String id = "mboard_id";
    String name = "mboard_name";
    String description = "description";
    String ram = "ram_id";
        String cpu = "cpu_id";
        String gpu = "gpu_id";
        
    //method to insert information into the given table
    public int insertMboard(TMboard tmboard){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            name + ", " + 
                            
                ram + ", " +
                cpu + ", " +
                gpu + ", " +
                 description+ ") values ('" + 
                            tmboard.getMboardName()+ "' , '" + 
                             tmboard.getRamId()+ "' , '" + 
                            tmboard.getCpuId()+ "' , '" + 
                            tmboard.getGpuId()+ "' , '" + 
                            tmboard.getDescription()+ "'); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMboardDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TMboardDao.class.toString());
            
        }
    }    
    
    public List<TMboard> selectAllTMboard(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TMboard> listTMboard;
        listTMboard = new ArrayList<TMboard>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TMboard tMboard =  new TMboard();
                tMboard.setMboardId(rs.getInt("id"));
                tMboard.setMboardName(rs.getString("name"));
                tMboard.setDescription(rs.getString("description"));
                listTMboard.add(tMboard);
            }
            return listTMboard;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMboardDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TMboardDao.class.toString());
            
        }
    }
    
    public TMboard selectTMboard(TMboard tMboard){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tMboard.getMboardId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tMboard.setMboardId(rs.getInt("id"));
            tMboard.setMboardName(rs.getString("name"));
            tMboard.setDescription(rs.getString("description"));
            return tMboard;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMboardDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TMboardDao.class.toString());
            
        }
    }
    
    public boolean deleteMboard(TMboard mboardDelete){
        
        String deleteSql = "DELETE FROM " + tableName + " WHERE " + id + " = " 
                + mboardDelete.getMboardId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMboardDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TMboardDao.class.toString());
            
        }
    }
    
    public boolean updateTMboard(TMboard mboardUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET "
                + name + " = " + mboardUpdate.getMboardName() 
                + description  + " = " + mboardUpdate.getDescription()+ ", "
                + " WHERE " + id + " = "  + mboardUpdate.getCpuId() + ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TMboardDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TMboardDao.class.toString());
            
        }
    }
}
          