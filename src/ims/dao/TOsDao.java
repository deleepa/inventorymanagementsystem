/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TOsDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "os";
    String id = "os_id";
    String name = "os_name";
    String version = "os_version";
        
    //method to insert information into the given table
    public int insertOs(TOs tOs){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            name + ", " + 

                            version + ") values ('" + 

                            tOs.getOsName()+ "' , '" +
                            tOs.getOsVersion()+ "'); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TOsDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TOsDao.class.toString());
            
        }
    }    
    
    public List<TOs> selectAllTOs(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TOs> listTOs = new ArrayList<>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TOs tOs =  new TOs();
                tOs.setOsId(rs.getInt(id));
                tOs.setOsName(rs.getString(name));
                tOs.setOsVersion(rs.getString(version));
                listTOs.add(tOs);
            }
            return listTOs;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TOsDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TOsDao.class.toString());
            
        }
    }
    
    public TOs selectTOs(TOs tOs){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tOs.getOsId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tOs.setOsId(rs.getInt(id));
            tOs.setOsName(rs.getString(name));
            tOs.setOsVersion(rs.getString(version));
            return tOs;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TOsDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TOsDao.class.toString());
            
        }
    }
    
    public TOs selectTOsFromId(int tOsId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tOsId +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TOs tOs = new TOs();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tOs.setOsId(rs.getInt(id));
            tOs.setOsName(rs.getString(name));
            tOs.setOsVersion(rs.getString(version));
            return tOs;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TOsDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TOsDao.class.toString());
            
        }
    }
    
    public boolean deleteOs(TOs osDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + id + " = " 
                + osDelete.getOsId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TOsDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TOsDao.class.toString());
            
        }
    }
    
    public boolean updateTOs(TOs tOsUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " 
                + name + " = '" + tOsUpdate.getOsName() + "', "
                + version + " = '" + tOsUpdate.getOsVersion() + "' "
                + " WHERE " + id + " = " + tOsUpdate.getOsId() +";"; 
        System.out.println("User is  "+updateSql);
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TOsDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TOsDao.class.toString());
            
        }
    }
}
          