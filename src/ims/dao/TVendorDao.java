/**
 * Author: MIJAEL  
 * Date:    17/03/2013
 * Project: TVedorDao
 * Description: TVendorDao generate the Data Access Connection  from the object Vendor.
 * 
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TVendorDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();

    String tableName = "vendor";
    String id = "vendor_id";
    String name = "vendor_name";
    String address = "vendor_address";
    String invoiceNo = "vendor_invoice_number";
    String contactNo = "vendor_phone";
        
    //method to insert information into the given table
    public int insertVendor(TVendor tvendor){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            name + ", " + 
                            address + ", " +
                            invoiceNo + ", " +
                            contactNo + ") values ('" + 
                
                            tvendor.getVendorName()+ "' , '" + 
                            tvendor.getAddress()+ "' , '" + 
                            tvendor.getInvoiceNo()+ "' , '" + 
                            tvendor.getContactNo()+ "')";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TVendorDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TVendorDao.class.toString());
            
        }
    }    
    
    public List<TVendor> selectAllVendor(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TVendor> listVendors = new ArrayList<TVendor>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TVendor tVendor =  new TVendor();
                tVendor.setVendorId(rs.getInt(id));
                tVendor.setVendorName(rs.getString(name));
                tVendor.setAddress(rs.getString(address));
                tVendor.setContactNo(rs.getString(contactNo));
                tVendor.setInvoiceNo(rs.getString(invoiceNo));
                listVendors.add(tVendor);
            }
            return listVendors;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TVendorDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TVendorDao.class.toString());
            
        }
    }
    public TVendor selectTVendor(TVendor tVendor){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tVendor.getVendorId()+ ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next(); 
            tVendor.setVendorId(rs.getInt(id));
            tVendor.setVendorName(rs.getString(name));
            tVendor.setAddress(rs.getString(address));
            tVendor.setContactNo(rs.getString(contactNo));
            tVendor.setInvoiceNo(rs.getString(invoiceNo));
            return tVendor;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TVendorDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TVendorDao.class.toString()); 
        }
    }
    
    public TVendor selectTVendorFromId(int tVendorId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tVendorId+ ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TVendor tVendor = new TVendor();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next(); 
            tVendor.setVendorId(rs.getInt(id));
            tVendor.setVendorName(rs.getString(name));
            tVendor.setAddress(rs.getString(address));
            tVendor.setContactNo(rs.getString(contactNo));
            tVendor.setInvoiceNo(rs.getString(invoiceNo));
            return tVendor;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TVendorDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TVendorDao.class.toString()); 
        }
    }
     
    public boolean deleteVendor(TVendor vendor){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE vendor_id = " 
                + vendor.getVendorId()  + ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " +TVendorDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TVendorDao.class.toString());
            
        }
    }
    
    public boolean udateVendor(TVendor vendor){
        
        String updateSql = "UPDATE " + tableName + " SET " + 
                name + " = '" + vendor.getVendorName() +"', "+
                 contactNo + " = '" + vendor.getContactNo()+"', "+
                 invoiceNo + " = '" + vendor.getInvoiceNo() +"', "+
                 address + " = '" + vendor.getAddress() +"' "+
                " WHERE "+id+" = " + vendor.getVendorId() +";"; 

        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TVendorDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TVendorDao.class.toString());
        }
    }
}
          