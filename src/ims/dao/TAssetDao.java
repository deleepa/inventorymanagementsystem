/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TBodyDao
 */
package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TAssetDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();

    String tableName = "asset";
    String userId = "user_id";
    String pcmainId = "pc_main_id";
    //String tModelModelId= "t_model_model_id";
    String maintId= "maintenance_id";
    String vendorId= "vendor_id";
    String id= "asset_id";

        
    //method to insert information into the given table
    public int insertTAsset(TAsset tAsset){
        
        String sqlInsert = "INSERT INTO " + tableName + " ( " +
                            userId + ", " +
                            pcmainId + ", " +
                            maintId + ", " +
                            vendorId + ") " + "values ('" + 
                            tAsset.gettUserId()+ "' , '" +
                            tAsset.getPcmainId()+ "' , '" +
                            tAsset.gettMaintenanceId()+ "' , '" +
                            tAsset.getVendorId()+ "'); ";

        String sqlSelect = "SELECT MAX(" +id +") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();

            if( !st.execute(sqlInsert) ){

                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAssetDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TAssetDao.class.toString());
            
        }
    }    
    
    public List<TAsset> selectAllTAsset(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TAsset> listTAsset = new ArrayList<TAsset>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TAsset tAsset =  new TAsset();
                tAsset.settUserId(rs.getInt(userId));
                tAsset.settMaintenanceId(rs.getInt(maintId));
                tAsset.setVendorId(rs.getInt(vendorId));
                tAsset.setPcmainId(rs.getInt(pcmainId));
                listTAsset.add(tAsset);
            }
            return listTAsset;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBodyDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TBodyDao.class.toString());
            
        }
    }
    
    public TAsset selectTAsset(TAsset tAsset){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE" + id + "= " 
                + tAsset.getAssetId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
                tAsset.settUserId(rs.getInt(userId));
                tAsset.settMaintenanceId(rs.getInt(maintId));
                tAsset.setVendorId(rs.getInt(vendorId));
                tAsset.setPcmainId(rs.getInt(pcmainId));
            return tAsset;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAssetDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TAssetDao.class.toString());
            
        }
    }
    
    public TAsset selectTAssetFromId(int assetId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + " = '" 
                + assetId+"';"; 
        System.out.println("TAsset select statement: " + selectRow);
        Connection c = connectionDB.getConnection();
        
        try {
            TAsset tAsset = new TAsset();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
                tAsset.setAssetId(rs.getInt(id));
                tAsset.settUserId(rs.getInt(userId));
                tAsset.settMaintenanceId(rs.getInt(maintId));
                tAsset.setVendorId(rs.getInt(vendorId));
                tAsset.setPcmainId(rs.getInt(pcmainId));
            return tAsset;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAssetDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TAssetDao.class.toString());
            
        }
    }
    
    public TAsset selectTAssetFromUserId(int searchUserId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + userId + " = '" 
                + searchUserId +"';"; 
        System.out.println("TAsset select statement: " + selectRow);
        Connection c = connectionDB.getConnection();
        
        try {
            TAsset tAsset = new TAsset();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tAsset.setAssetId(rs.getInt(id));
            tAsset.settUserId(rs.getInt(userId));
            tAsset.settMaintenanceId(rs.getInt(maintId));
            tAsset.setVendorId(rs.getInt(vendorId));
            tAsset.setPcmainId(rs.getInt(pcmainId));
            return tAsset;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAssetDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TAssetDao.class.toString());
            
        }
    }
    
    public boolean deleteTAsset(TAsset tAssetDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE" + id + " = " 
                + tAssetDelete.getAssetId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAssetDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TAssetDao.class.toString());
            
        }
    }
    
        public boolean deleteTAssetByUserId(int userId){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + this.userId + " = '" 
                + userId +"';"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return !st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAssetDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TAssetDao.class.toString());
            
        }
    }
    
    public boolean updateTAsset(TAsset tAssetUpdate){
        
       String updateSql = "UPDATE " + tableName + " SET "
                + userId  + " = "+ tAssetUpdate.gettUserId()+ ", "
                + pcmainId  + " = "+ tAssetUpdate.getPcmainId()+ ", "
              //  + tModelModelId  + " = "+ tAssetUpdate.gettModelModelId()+ ", "

                + maintId  + " = "+ tAssetUpdate.gettMaintenanceId()+ ", "
                + vendorId + " = " + tAssetUpdate.getVendorId()
                + " WHERE " + id + " = "  + tAssetUpdate.getAssetId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TAssetDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TAssetDao.class.toString());
            
        }
    }
}
          
