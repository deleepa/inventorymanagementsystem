/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TBodyDao
 */
package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TBodyDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "t_body";
    String serialNo = "serial_no";
    String description = "desc";
    String id = "body_id";
    String hddId = "hdd_id";
    String mboardId = "mboard_id";
    String osId = "os_id";
    String pcClassId = "pc_class_id";

    //method to insert information into the given table
    public int insertTBody(TBody tBody){
        
        String sqlInsert = "INSERT INTO " + tableName + " ( " +

                            serialNo + ", `" +
                            description + "` , " +
                            hddId + ", " +
                            mboardId + ", " +
                            osId + ", " +
                            pcClassId + ")  values ('" + 

                            tBody.getSerialNo() + "' , '" +
                            tBody.getDesc() + "' , '" +
                            tBody.getHddId() + "' , '" +
                            tBody.getMboardId() + "' , '" +
                            tBody.getOsId() + "' , '" +
                            tBody.getPcClassId() + "'); ";
        String sqlSelect = "SELECT MAX(" +id +") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            st.execute(sqlInsert) ;
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
          
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBodyDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TBodyDao.class.toString());
            
        }
    }    
    
    public List<TBody> selectAllTBody(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TBody> listTBody = new ArrayList<TBody>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TBody tBody =  new TBody();
                tBody.setBodyId(rs.getInt("tBody_id"));
                tBody.setDesc(rs.getString("tBody_name"));
                tBody.setOsId(rs.getInt("tBody_osId"));
                tBody.setHddId(rs.getInt("tBody_hddId"));
                tBody.setMboardId(rs.getInt("tBody_mboardId"));
                tBody.setPcClassId(rs.getInt("tBody_pcClassId"));
                tBody.setSerialNo(rs.getString("tBody_serialNo"));
                listTBody.add(tBody);
            }
            return listTBody;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBodyDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TBodyDao.class.toString());
            
        }
    }
    
    public TBody selectTBody(TBody tBody){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE" + id + "= " 
                + tBody.getBodyId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tBody.setBodyId(rs.getInt("tBody_id"));
            tBody.setDesc(rs.getString("tBody_name"));
            tBody.setOsId(rs.getInt("tBody_osId"));
            tBody.setHddId(rs.getInt("tBody_hddId"));
            tBody.setMboardId(rs.getInt("tBody_mboardId"));
            tBody.setPcClassId(rs.getInt("tBody_pcClassId"));
            tBody.setSerialNo(rs.getString("tBody_serialNo"));
            return tBody;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBodyDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TBodyDao.class.toString());
            
        }
    }
    
    public boolean deleteTBody(TBody tBodyDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE" + id + " = " 
                + tBodyDelete.getBodyId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBodyDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TBodyDao.class.toString());
            
        }
    }
    
    public boolean updateTBody(TBody tBodyUpdate){
        
       String updateSql = "UPDATE " + tableName + " SET "
                + serialNo  + " = "+ tBodyUpdate.getSerialNo()+ ", "
                + description  + " = "+ tBodyUpdate.getDesc()+ ", "
                + hddId  + " = "+ tBodyUpdate.getHddId()+ ", "
                + mboardId  + " = "+ tBodyUpdate.getMboardId()+ ", "
                + osId  + " = "+ tBodyUpdate.getOsId()+ ", "
                + pcClassId + " = " + tBodyUpdate.getPcClassId()
                + " WHERE " + id + " = "  + tBodyUpdate.getBodyId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TBodyDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TBodyDao.class.toString());
            
        }
    }
}
          
