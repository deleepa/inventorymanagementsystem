/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TPeripheralsDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();

    String tableName = "peripheral";
    String id = "peripheral_id";
    String type = "peripheral_type";
    String serialNumber = "peripheral_serial_no";
    String description = "peripheral_description";
        
    //method to insert information into the given table
    public int insertPeripherals(TPeripherals tperipherals){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            type + ", " + 
                            serialNumber + ", `" +
                            description + "`) values ('" + 
                            tperipherals.getType() + "' , '" + 
                            tperipherals.getPerSerialNo() + "' , '" + 
                            tperipherals.getDesc() + "'); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPeripheralsDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TPeripheralsDao.class.toString());
            
        }
    }    
    
    public List<TPeripherals> selectAllPeripherals(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TPeripherals> listPeripherals = new ArrayList<TPeripherals>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TPeripherals peripherals =  new TPeripherals();
                peripherals.setPeriId(rs.getInt(id));
                peripherals.setType(rs.getString(type));
                peripherals.setPerSerialNo(rs.getString(serialNumber));
                peripherals.setDesc(rs.getString(description));
                listPeripherals.add(peripherals);
            }
            return listPeripherals;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPeripheralsDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TPeripheralsDao.class.toString());
            
        }
    }
    
    public TPeripherals selectPeripheral(TPeripherals peripherals){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + peripherals.getPeriId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            peripherals.setPeriId(rs.getInt(id));
            peripherals.setType(rs.getString(type));
            peripherals.setPerSerialNo(rs.getString(serialNumber));
            peripherals.setDesc(rs.getString(description));
            return peripherals;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPeripheralsDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TPeripheralsDao.class.toString());
            
        }
    }
    
        public TPeripherals selectPeripheralFromId(int peripheralId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + peripheralId +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TPeripherals peripherals = new TPeripherals();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            peripherals.setPeriId(rs.getInt(id));
            peripherals.setType(rs.getString(type));
            peripherals.setPerSerialNo(rs.getString(serialNumber));
            peripherals.setDesc(rs.getString(description));
            return peripherals;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPeripheralsDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TPeripheralsDao.class.toString());
            
        }
    }
    
    public boolean deletePeripherals(TPeripherals peripheralsDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE" + id +" = "  
                + peripheralsDelete.getPeriId()+ ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPeripheralsDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TPeripheralsDao.class.toString());      
        }
    }
    
   public boolean updatePeripherals(TPeripherals peripheralsUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " +
                        type + " = '" + peripheralsUpdate.getType() +"', "+
                        serialNumber + " = '" + peripheralsUpdate.getPerSerialNo() +"', "+
                        description + " = '" + peripheralsUpdate.getDesc()+"' "+
                           " WHERE " + id + " = " + peripheralsUpdate.getPeriId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);           
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPeripheralsDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TPeripheralsDao.class.toString());   
        }
    }
}

