/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TBodyDao
 */
package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TCpuDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "cpu";
    String id = "cpu_id";
    String brand = "cpu_brand";
    String speed = "cpu_speed";
        
    //method to insert information into the given table
    public int insertCPU(TCpu tcpu){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            //id + ", " +
                            brand + ", " +
                            speed +
                            ") values ('" + 
                            tcpu.getCpuBrand()+ "' , '" + 
                            tcpu.getSpeed() + "' ); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TCpuDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TCpuDao.class.toString());   
        }
    }    
    
   public List<TCpu> selectAllTCpu(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TCpu> listTCpu;
        listTCpu = new ArrayList<>();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TCpu tCpu =  new TCpu();
                tCpu.setCpuId(rs.getInt(id));
                tCpu.setCpuBrand(rs.getString(brand));
                tCpu.setSpeed(rs.getString(speed));
                listTCpu.add(tCpu);
            }
            return listTCpu;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TCpuDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TCpuDao.class.toString());
        }
    }
    
    public TCpu selectTCpu(TCpu tCpu){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tCpu.getCpuId() + ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next(); 
            tCpu.setCpuId(rs.getInt(id));
            tCpu.setCpuBrand(rs.getString(brand));
            tCpu.setSpeed(rs.getString(speed));
            return tCpu;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TCpuDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TCpuDao.class.toString()); 
        }
    }
    
        public TCpu selectTCpuFromId(int tCpuId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tCpuId + ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TCpu tCpu = new TCpu();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next(); 
            tCpu.setCpuId(rs.getInt(id));
            tCpu.setCpuBrand(rs.getString(brand));
            tCpu.setSpeed(rs.getString(speed));
            return tCpu;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TCpuDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TCpuDao.class.toString()); 
        }
    }
    
    public boolean deleteCpu(TCpu cpuDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + id + " = "
                + cpuDelete.getCpuId() +";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
        } catch (SQLException ex) {
            System.err.print("Problems in " + TCpuDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TCpuDao.class.toString());
        }
    }
    public boolean updateTCpu(TCpu tCpuUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET "
                + brand + " = '" + tCpuUpdate.getCpuBrand() + "', "
                + speed + " = '" + tCpuUpdate.getSpeed()
                + "' WHERE " + id + " = "  + tCpuUpdate.getCpuId() +";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
        } catch (SQLException ex) {
            System.err.print("Problems in " + TCpuDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TCpuDao.class.toString());   
        }
    }
}
          