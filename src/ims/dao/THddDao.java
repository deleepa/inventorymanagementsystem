/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class THddDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "hdd";
    String id = "hdd_id";
    String capacity = "hdd_capacity";
    String brand = "hdd_brand";
        
    //method to insert information into the given table
    public int insertHdd(THdd thdd){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            capacity + ", " + 
                            brand + ") values ('" + 
                            thdd.getHddCapacity()+ "' , '" + 
                            thdd.getHddBrand()+ "'); ";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if( !st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + THddDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, THddDao.class.toString());
            
        }
    }    
    
    public List<THdd> selectAllTHdd(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<THdd> listTHdd;
        listTHdd = new ArrayList<>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                THdd tHdd =  new THdd();
                tHdd.setHddId(rs.getInt(id));
                tHdd.setHddCapacity(rs.getString(capacity));
                tHdd.setHddBrand(rs.getString(brand));
                listTHdd.add(tHdd);
            }
            return listTHdd;
        } catch (SQLException ex) {
            System.err.print("Problems in " + THddDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, THddDao.class.toString());
            
        }
    }
    
    public THdd selectTHdd(THdd tHdd){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tHdd.getHddId() +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tHdd.setHddId(rs.getInt(id));
            tHdd.setHddCapacity(rs.getString(capacity));
            tHdd.setHddBrand(rs.getString(brand));
            return tHdd;
        } catch (SQLException ex) {
            System.err.print("Problems in " + THddDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, THddDao.class.toString());
            
        }
    }
    
    public THdd selectTHddFromId(int tHddId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id + "= " 
                + tHddId +";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            THdd tHdd = new THdd();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            tHdd.setHddId(rs.getInt(id));
            tHdd.setHddCapacity(rs.getString(capacity));
            tHdd.setHddBrand(rs.getString(brand));
            return tHdd;
        } catch (SQLException ex) {
            System.err.print("Problems in " + THddDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, THddDao.class.toString());
            
        }
    }
    
    public boolean deleteTHdd(THdd tHddDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE " + id + " = " 
                + tHddDelete.getHddId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + THddDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, THddDao.class.toString());
            
        }
    }
    
    public boolean updateTHdd(THdd tHddUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " 
                + capacity + "= '"+ tHddUpdate.getHddCapacity()+ "', "
                + brand + "= '"+ tHddUpdate.getHddBrand()+ "' "
                + " WHERE "+ id + "= " + tHddUpdate.getHddId()+ ";";           
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + THddDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, THddDao.class.toString());
            
        }
    }
}
          