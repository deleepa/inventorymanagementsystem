/**
 * Authour: Deleepa
 * Date:    17/03/2013
 * Project: TGpuDao
 */

package ims.dao;

import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import ims.sqlClasses.*;

public class TPcClassDao {
    
    //creates a connection to the SQL database
    ConnectionDB connectionDB = new ConnectionDB();
    //Connection c = connectionDB.getConnection();
    
    String tableName = "pc_class";
    String id = "pc_class_id";
    String name = "pc_class_name";
        
    //method to insert information into the given table
    public int insertPcclass(TPcClass tpcclass){
        
        String sqlInsert = "INSERT INTO " + tableName + " (" +
                            name + ") values ('" + 
                            tpcclass.getClassName() + "');";
        String sqlSelect = "SELECT MAX(" + id + ") FROM " + tableName + ";";
        Connection c = connectionDB.getConnection();
        try {
            Statement st = c.createStatement();
            if(!st.execute(sqlInsert) ){
                ResultSet rs = st.executeQuery(sqlSelect);
                rs.next();
                return rs.getInt(1);
            }
            return 0;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcClassDao.class.toString() + " error " + ex);
            return 0;
        } finally {
            connectionDB.closeConnection(c, TPcClassDao.class.toString());
            
        }
    }    
    
    public List<TPcClass> selectAllPcclass(){
        
        String sqlSelectAll = "SELECT * FROM " +tableName + ";"; 
        Connection c = connectionDB.getConnection();
        List<TPcClass> listPcclass = new ArrayList<TPcClass>();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sqlSelectAll);
            while(rs.next()){
                TPcClass pcclass =  new TPcClass();
                pcclass.setPcClassId(rs.getInt(id));
                pcclass.setClassName(rs.getString(name));
                listPcclass.add(pcclass);
            }
            return listPcclass;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcClassDao.class.toString() + " error " + ex);
            return null;
            
        } finally {
            connectionDB.closeConnection(c, TPcClassDao.class.toString());
            
        }
    }
    
    public TPcClass selectPcClass(TPcClass pcclass){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + pcclass.getPcClassId()+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            pcclass.setPcClassId(rs.getInt(id));
            pcclass.setClassName(rs.getString(name));
            return pcclass;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcClassDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TPcClassDao.class.toString());
            
        }
    }
    
    public TPcClass selectPcClassFromId(int pcClassId){
        
        String selectRow = "SELECT * FROM " + tableName + " WHERE " + id +" = " 
                + pcClassId+";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            TPcClass pcclass = new TPcClass();
            Statement st = connectionDB.getConnection().createStatement();
            ResultSet rs = st.executeQuery(selectRow);
            rs.next();
            pcclass.setPcClassId(rs.getInt(id));
            pcclass.setClassName(rs.getString(name));
            return pcclass;
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcClassDao.class.toString() + " error " + ex);
            return null;
        } finally {
            connectionDB.closeConnection(c, TPcClassDao.class.toString());
            
        }
    }
    
    public boolean deletePcclass(TPcClass pcclassDelete){
        
        String deleteSql = "DELETE FROM " + tableName +" WHERE" + id +" = "  
                + pcclassDelete.getPcClassId()+ ";"; 
        Connection c = connectionDB.getConnection();
        
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(deleteSql);
            
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcClassDao.class.toString() + " error " + ex);
            return false;
            
        } finally {
            connectionDB.closeConnection(c, TPcClassDao.class.toString());      
        }
    }
    
   public boolean updatePcclass(TPcClass pcclassUpdate){
        
        String updateSql = "UPDATE " + tableName + " SET " 
                +  name + " = '" + pcclassUpdate.getClassName()+
                           "' WHERE " + id + " = " + pcclassUpdate.getPcClassId()+";"; 
        Connection c = connectionDB.getConnection();
        try {
            Statement st = connectionDB.getConnection().createStatement();
            return st.execute(updateSql);           
        } catch (SQLException ex) {
            System.err.print("Problems in " + TPcClassDao.class.toString() + " error " + ex);
            return false;
        } finally {
            connectionDB.closeConnection(c, TPcClassDao.class.toString());   
        }
    }
}
