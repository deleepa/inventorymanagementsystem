/**
 * Authour: Deleepa
 * Date:    01/06/2013
 * Project: InventoryManagementSystem  |  ServicePopulateInsertForm
 */

package ims.service;

import ims.GUIInterface.GUIInsertFromUpdate;
import ims.dao.*;
import ims.sqlClasses.*;
import java.util.HashMap;

public class ServicePopulateInsertForm {
    /*
     * create all the beans that must be mapped
     */
    TAsset asset = new TAsset();
    TMaintenance maintenance = new TMaintenance();
    TUser user = new TUser();
    TVendor vendor = new TVendor();
    TPcMain pcMain = new TPcMain();
    TGpu gpu = new TGpu();
    THdd hdd = new THdd();
    TCpu cpu = new TCpu();
    TOs os = new TOs();
    TRam ram = new TRam();
    TPcClass pcClass = new TPcClass();
    TPeripherals peripheral = new TPeripherals();
    TMonitor monitor = new TMonitor();
    
    /*
     * create all the dao's that will be used
     */
    TAssetDao assetDao = new TAssetDao();
    TMaintenanceDao maintenanceDao = new TMaintenanceDao();
    TUserDao userDao = new TUserDao();
    TVendorDao vendorDao = new TVendorDao();
    TPcMainDao pcmainDao = new TPcMainDao();
    TGpuDao gpuDao = new TGpuDao();
    THddDao hddDao = new THddDao();
    TCpuDao cpuDao = new TCpuDao();
    TOsDao osDao = new TOsDao();
    TRamDao ramDao = new TRamDao();
    TPcClassDao pcclassDao = new TPcClassDao();
    TPeripheralsDao peripheralDao = new TPeripheralsDao();
    TMonitorDao monitorDao = new TMonitorDao();
            
    //constructor
    public ServicePopulateInsertForm(TAsset assetFromUpdate) {
        
        try{
            System.out.println("assetFromUpdate " + assetFromUpdate);
            //map the asset object first
            asset = assetDao.selectTAssetFromId(assetFromUpdate.getAssetId());
            
            //next map the tables that are connected to that particular asset record
            maintenance = maintenanceDao.selectTMaintenanceFromId(asset.gettMaintenanceId());
            
            user = userDao.selectTUserFromId(asset.gettUserId());
            vendor = vendorDao.selectTVendorFromId(asset.getVendorId());
            pcMain = pcmainDao.selectTPcMainFromId(asset.getPcmainId());
            
            /*
             * after pcmain is mapped, the rest of the tables that are
             * connected to the pcMain record can be mapped
             */
            
            gpu = gpuDao.selectTGpuFromId(pcMain.getGpuId());
            hdd = hddDao.selectTHddFromId(pcMain.getHddId());
            cpu = cpuDao.selectTCpuFromId(pcMain.getCpuId());
            os = osDao.selectTOsFromId(pcMain.getOsId());
            ram = ramDao.selectRamFromId(pcMain.getRamId());

            pcClass = pcclassDao.selectPcClassFromId(pcMain.getPcClassId());
           
            peripheral = peripheralDao.selectPeripheralFromId(pcMain.getPeriId());
            
            monitor = monitorDao.selectTMonitorFromId(pcMain.getMonitorId());
            
            
            
            
        } catch (Exception e) {
            System.out.println("Fatal error occured in " + ServicePopulateInsertForm.class.toString() +
                               " at runtime. Error: " + e);
           
        } finally {
            
        }
    }
    
    public HashMap<String, Object> mapBeans(){
        //put all the beans in a hashmap to pass to the GUI Interface
        HashMap<String, Object> beans = new HashMap<String, Object>();
        
        beans.put("asset", asset);
        beans.put("maintenance", maintenance);
        beans.put("user", user);
        beans.put("vendor", vendor);
        beans.put("pcMain", pcMain);
        beans.put("gpu", gpu);
        beans.put("hdd", hdd);
        beans.put("cpu", cpu);
        beans.put("os", os);
        beans.put("ram", ram);
        beans.put("pcClass", pcClass);
        beans.put("peripheral", peripheral);
        beans.put("monitor", monitor);
        
        return beans;
    }
    
    public static void main(String[] args) {
        TAsset testAsset = new TAsset();
        testAsset.setAssetId(2);
        
        ServicePopulateInsertForm callService = new ServicePopulateInsertForm(testAsset);
        
        GUIInsertFromUpdate testInsert = new GUIInsertFromUpdate(callService.mapBeans());
        testInsert.setVisible(true);
    }
}
