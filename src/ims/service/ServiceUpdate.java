/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ims.service;

import ims.dao.TAssetDao;
import ims.dao.TCpuDao;
import ims.dao.TGpuDao;
import ims.dao.THddDao;
import ims.dao.TMaintenanceDao;
import ims.dao.TMonitorDao;
import ims.dao.TOsDao;
import ims.dao.TPcClassDao;
import ims.dao.TPeripheralsDao;
import ims.dao.TRamDao;
import ims.dao.TUserDao;
import ims.dao.TVendorDao;
import ims.sqlClasses.TAsset;
import ims.sqlClasses.TCpu;
import ims.sqlClasses.TGpu;
import ims.sqlClasses.THdd;
import ims.sqlClasses.TMaintenance;
import ims.sqlClasses.TMonitor;
import ims.sqlClasses.TOs;
import ims.sqlClasses.TPcClass;
import ims.sqlClasses.TPcMain;
import ims.sqlClasses.TPeripherals;
import ims.sqlClasses.TRam;
import ims.sqlClasses.TUser;
import ims.sqlClasses.TVendor;
import java.util.Map;

/**
 *
 * @author MIJAEL
 */
public class ServiceUpdate {
      /*
     * Declare all the necessary beans here
     */
    TAsset asset;
    TMaintenance maintenance;
    TUser user;
    TVendor vendor;
    TPcMain pcMain;
    TGpu gpu;
    THdd hdd;
    TCpu cpu;
    TOs os;
    TRam ram;
    TPcClass pcClass;
    TPeripherals peripherals;
    TMonitor monitor;
    
    /*
     * Declare the necessary dao's here
     */
    TAssetDao assetDao = new TAssetDao();
    TUserDao userDao = new TUserDao(); 
    TVendorDao vendorDao = new TVendorDao();
    TMaintenanceDao maintenanceDao = new TMaintenanceDao();
    TCpuDao cpuDao = new TCpuDao();
    TGpuDao gpuDao = new TGpuDao();
    TOsDao osDao = new TOsDao();
    TRamDao ramDao = new TRamDao();
    TPcClassDao pcClassDao = new TPcClassDao();
    TPeripheralsDao peripheralsDao = new TPeripheralsDao();
    TMonitorDao monitorDao = new TMonitorDao();
    THddDao hddDao = new THddDao();
    
    public boolean updateEquipment(Map<String,Object> mapBeans){
       setBeans(mapBeans);
        try {
           cpuDao.updateTCpu(cpu);
            gpuDao.udateTGpu(gpu);
            hddDao.updateTHdd(hdd);
            maintenanceDao.udateTMaintenance(maintenance);
            monitorDao.updateTMonitor(monitor);
            osDao.updateTOs(os);
            pcClassDao.updatePcclass(pcClass);
            peripheralsDao.updatePeripherals(peripherals);
            ramDao.updateRam(ram);
            userDao.udateUser(user);
            vendorDao.udateVendor(vendor); 
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        
    }
    
     private void setBeans(Map<String,Object> mapBeans){

        cpu = (TCpu) mapBeans.get("cpu");
        gpu = (TGpu) mapBeans.get("gpu");
        hdd = (THdd) mapBeans.get("hdd");
        maintenance = (TMaintenance) mapBeans.get("maintenance");
        monitor = (TMonitor) mapBeans.get("monitor");
        os = (TOs) mapBeans.get("os");
        pcClass = (TPcClass) mapBeans.get("pcClass");
        pcMain = (TPcMain) mapBeans.get("pcMain");
        peripherals = (TPeripherals) mapBeans.get("peripheral");
        ram = (TRam) mapBeans.get("ram");
        vendor = (TVendor) mapBeans.get("vendor");
        user = (TUser) mapBeans.get("user");
    }
}
