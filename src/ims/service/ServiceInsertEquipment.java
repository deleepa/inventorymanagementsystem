package ims.service;

import ims.dao.*;
import ims.sqlClasses.*;
import java.util.Map;

/*
 *This class will be invoked at run time by the GUIInsert class.
 *A hash map of the beans will be passed to this class. 
 */
public class ServiceInsertEquipment {
    
    TAsset asset;
    TCpu cpu;
    TGpu gpu;
    THdd hdd;
    TMaintenance maintenance;
    TMonitor monitor;
    TOs os;
    TPcClass pcClass;
    TPcMain pcMain;
    TPeripherals peripheral;
    TRam ram;
    TVendor vendor;
    TUser user;
    
    /*
     * Here the method will receive a map of beans that will be mapped to 
     * this class's own set of beans in the setBeans() method
     * These beans then can be passed onto the respective DAO's to be written 
     * into the database
     */
    public int insertEquipment(Map<String, Object> mapBeans) {
        
        //map the beans before begining the insert process
        setBeans(mapBeans);
        
        TUserDao userDao = new TUserDao(); 
        TVendorDao vendorDao = new TVendorDao();
        TMaintenanceDao maintenanceDao = new TMaintenanceDao();
        TCpuDao cpuDao = new TCpuDao();
        TGpuDao gpuDao = new TGpuDao();
        TOsDao osDao = new TOsDao();
        TRamDao ramDao = new TRamDao();
        TPcClassDao pcClassDao = new TPcClassDao();
        TPeripheralsDao peripheralsDao = new TPeripheralsDao();
        TMonitorDao monitorDao = new TMonitorDao();
        THddDao hddDao = new THddDao();

        TAsset asset = new TAsset();
        TAssetDao assetDao = new TAssetDao();

        TPcMain pcMain = new TPcMain();
        TPcMainDao pcMainDao =new TPcMainDao();
        
        int insertedAssetID = 0;
        
        try {
            /*
             * Insert all the records required for pcmain table first
             * Return the ID that was inserted and set that in the pcmain bean
             */
            pcMain.setGpuId(gpuDao.insertTGpu(gpu));
            pcMain.setHddId(hddDao.insertHdd(hdd));
            pcMain.setCpuId(cpuDao.insertCPU(cpu));
            pcMain.setOsId(osDao.insertOs(os));
            pcMain.setRamId(ramDao.insertRam(ram));
            pcMain.setMonitorId(monitorDao.insertMonitor(monitor));
            pcMain.setPeriId(peripheralsDao.insertPeripherals(peripheral));
            pcMain.setPcClassId(pcClassDao.insertPcclass(pcClass));       

            /*
             * set all the records required for asset record
             * return the ids that were insert and set in the asset bean
             * including the pcmain records
             */
            asset.settMaintenanceId(maintenanceDao.insertMaintenance(maintenance));
            asset.settUserId(userDao.insertUser(user));
            asset.setVendorId(vendorDao.insertVendor(vendor));
            asset.setPcmainId(pcMainDao.insertPcMain(pcMain));

            //insert into the asset table
            insertedAssetID = assetDao.insertTAsset(asset);

            System.out.println("New device has been inserted sucessfully..");
        } 
        catch (Exception e) {
            System.out.println("Error occured: " + e.getMessage());
        }
        finally {
            return insertedAssetID;
        }
    }
    
    /*
     * Here a hash map is passed onto the method that will proceed to map it to
     * the beans have been create above as class variables. 
     */
    private void setBeans(Map<String,Object> mapBeans){

        cpu = (TCpu) mapBeans.get("cpu");
        gpu = (TGpu) mapBeans.get("gpu");
        hdd = (THdd) mapBeans.get("hdd");
        maintenance = (TMaintenance) mapBeans.get("maintenance");
        monitor = (TMonitor) mapBeans.get("monitor");
        os = (TOs) mapBeans.get("os");
        pcClass = (TPcClass) mapBeans.get("pcClass");
        pcMain = (TPcMain) mapBeans.get("pcMain");
        peripheral = (TPeripherals) mapBeans.get("peripheral");
        ram = (TRam) mapBeans.get("ram");
        vendor = (TVendor) mapBeans.get("vendor");
        user = (TUser) mapBeans.get("user");
    }

}