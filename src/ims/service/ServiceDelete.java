/**
 * Authour: Deleepa
 * Date:    10/06/2013
 * Project: InventoryManagementSystem  |  ServiceDelete
 */

package ims.service;

import ims.dao.*;
import ims.sqlClasses.*;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;


public class ServiceDelete {
    
    /*
     * Declare all the necessary beans here
     */
    TAsset asset;
    TMaintenance maintenance;
    TUser user;
    TVendor vendor;
    TPcMain pcMain;
    TGpu gpu;
    THdd hdd;
    TCpu cpu;
    TOs os;
    TRam ram;
    TPcClass pcClass;
    TPeripherals peripherals;
    TMonitor monitor;
    
    /*
     * Declare the necessary dao's here
     */
    TAssetDao assetDao = new TAssetDao();
    TUserDao userDao = new TUserDao(); 
    TVendorDao vendorDao = new TVendorDao();
    TMaintenanceDao maintenanceDao = new TMaintenanceDao();
    TCpuDao cpuDao = new TCpuDao();
    TGpuDao gpuDao = new TGpuDao();
    TOsDao osDao = new TOsDao();
    TRamDao ramDao = new TRamDao();
    TPcClassDao pcClassDao = new TPcClassDao();
    TPeripheralsDao peripheralsDao = new TPeripheralsDao();
    TMonitorDao monitorDao = new TMonitorDao();
    THddDao hddDao = new THddDao();
    
    
    public boolean copyToTextFile(){
       
        //NICOLAI!! write the backup logic here!
        //you dont need to receive a hashmap here
        //you can map any additional beans you want in the mapBeans method..
        //so you can use them straightaway 
        //mapBeans(assetId);
        try{
            OutputStreamWriter writer = new OutputStreamWriter(
            new FileOutputStream("UserDetails.txt", true), "UTF-8");
            BufferedWriter fbw = new BufferedWriter(writer);
            fbw.write("\n" + " User Name: " + user.getUserName()
                           + " Staff ID: " + user.getStaffId()
                           + "Maintenance Technician: " + maintenance.getTechnician()
                           + "________________________________");
            fbw.newLine();
            fbw.close();
        }catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        finally{
             System.out.println("information added");
        }
        return false;
    }
    
    public boolean deleteRecord(int assetId){
        
        mapBeans(assetId);
        
        boolean deletedFromAsset = assetDao.deleteTAssetByUserId(user.getUserId());
        boolean deletedFromUser = userDao.deleteUser(user);         
        
        if(deletedFromAsset && deletedFromUser) {
            return true;
        
        } else {
            return false;
        }
        
    }
    
    public void mapBeans(int assetId){        
        
        asset = assetDao.selectTAssetFromId(assetId);
        user = userDao.selectTUserFromId(asset.gettUserId());
//        vendor = vendorDao.selectTVendor(asset.);
//        pcMain;
//        gpu;
//        hdd;
//        cpu;
//        os;
//        ram;
//        pcClass;
//        peripherals;
//        monitor;
    }
}
