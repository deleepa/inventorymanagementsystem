package ims.service;

import ims.dao.*;
import java.util.Map;
import ims.util.ConnectionDB;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import ims.sqlClasses.*;
import java.sql.Array;
import java.util.HashMap;

public class ServiceSearch {
    TAsset asset;
    TCpu cpu;
//    TGpu gpu;
//    THdd hdd;
//    TMaintenance maintenance;
//    TMonitor monitor;
//    TOs os;
//    TPcClass pcClass;
    TPcMain pcMain;
//    TPeripherals peripheral;
    TRam ram;
    TVendor vendor;
    TUser user;  
    
    //Declare a hashmap that will be used by the entire class
    HashMap<Integer, TUser> userMap;
    
    //Declare the dao's that will be used
    TUserDao userDao = new TUserDao();
    TAssetDao assetDao = new TAssetDao();
    TVendorDao vendorDao = new TVendorDao();
    TRamDao ramDao = new TRamDao();
    TPcMainDao pcMainDao = new TPcMainDao();
    TCpuDao cpuDao = new TCpuDao();
    
    public ServiceSearch(String query){
        
        /* The string that is enterted must be matched to a record
           and then that TUser bean must be used to return any asset objects 
           as well */
        
        //declare the dao's
                 
        if(!query.equals("")){
            userMap = userDao.selectTUserByName(query);
            System.out.println("userMap size: " + userMap.size());
        }         
    }
    
    /*
     * This method is used to send an integer array of asset ids.
     * A temp TUser object is assigned each TUser object from the
     * hashmap. The assetDao is used to retreive the TAsset associated with
     * the userID, which is used to set each assetID in the array.
     */
    public int[] getAssetIds() {
        
        int index = 0;
        TUser tempTUser;
        int[] assetIds = new int[userMap.size()];
        
        while(index < userMap.size()) {
            tempTUser = userMap.get(index);
            asset =  assetDao.selectTAssetFromUserId(tempTUser.getUserId());
            assetIds[index] = asset.getAssetId();
            
            index++;
        }
        return assetIds;
        
    }
    
    /*
     * This method is used to assign the vendor names into a string array.
     * The TAssets are feteched similar to the getAssetIds()
     * Then the vendorId from each TAsset is used to fetch the relevant vendor name
     */
    
    public String[] getVendorNames(){
        
        int index = 0;
        TUser tempTUser;
        String[] vendorNames = new String[userMap.size()];
        
        while(index < userMap.size()) {
            tempTUser = userMap.get(index);
            asset = assetDao.selectTAssetFromUserId(tempTUser.getUserId());
            vendor = vendorDao.selectTVendorFromId(asset.getVendorId());
            vendorNames[index] = vendor.getVendorName();
            
            index++;
        }
        return vendorNames;
    }
    
    /*
     * This method works similar to the getVendorNames, except for 
     * one addition. The ram values have to be feteched through the 
     * PCMain table. 
     * Therefore, first the PCMain bean has to be retreived, and then
     * the relevant RAM bean will retreived.
     */
    
    public String[] getRamValues(){
        
        int index = 0;
        TUser tempTUser;
        String[] ramValues = new String[userMap.size()];
        
        while(index < userMap.size()) {
            tempTUser = userMap.get(index);
            asset = assetDao.selectTAssetFromUserId(tempTUser.getUserId());
            pcMain = pcMainDao.selectTPcMainFromId(asset.getPcmainId());
            ram = ramDao.selectRamFromId(pcMain.getRamId());
            ramValues[index] = ram.getRamCapacity();
            
            index++;
        }
        return ramValues;
    }
    
    /*
     * This method works similar to the getRamValues() method
     */
    public String[] getCpuBrand(){
        
        int index = 0;
        TUser tempTUser;
        String[] cpuBrands = new String[userMap.size()];
        
        while(index < userMap.size()) {
            tempTUser = userMap.get(index);
            asset = assetDao.selectTAssetFromUserId(tempTUser.getUserId());
            pcMain = pcMainDao.selectTPcMainFromId(asset.getPcmainId());
            cpu = cpuDao.selectTCpuFromId(pcMain.getCpuId());
            cpuBrands[index] = cpu.getCpuBrand();
            
            index++;
        }
        return cpuBrands;
    }
    
        /*
     * This method works similar to the getRamValues() method
     */
    public String[] getUsers(){
        
        int index = 0;
        TUser tempTUser;
        String[] users = new String[userMap.size()];
        
        while(index < userMap.size()) {
            tempTUser = userMap.get(index);
            users[index] = tempTUser.getUserName();
            
            index++;
        }
        return users;
    }
    
    public static void main(String[] args) {
        ServiceSearch serviceCall = new ServiceSearch("John Smith");
        
        int[] arrayIds = serviceCall.getAssetIds();
        String[] vendorNames = serviceCall.getVendorNames();
        String[] ramValue = serviceCall.getRamValues();
        String[] cpuBrands = serviceCall.getCpuBrand();
        String[] users = serviceCall.getUsers();
        
        for(int i = 0; i < arrayIds.length; i++) {
            System.out.println("arrayIds: " + arrayIds[i]);
            System.out.println("vendor names: " + vendorNames[i]);
            System.out.println("ram values: " + ramValue[i]);
            System.out.println("cpu values: " + cpuBrands[i]);
            System.out.println("users: " + users[i]);
        }
        
        
    }
}
