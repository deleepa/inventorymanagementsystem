--record 1--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('Nvidia', '1GB', 'Dedicated');

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('500GB', 'Hitachi');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel', '1.7Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('WIndows 7', 'SP1');

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Kingston', 'DDR3', '2GB', '2');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Desktop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (1, 1, 1, 1, 1, 1, 1, 1);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('ABC Computers', '123, Burslem Drive', '67543D', '0865487524');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('John Smith', '1234');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES ('Kevin', '12/06/2013', 'Fixed a burnt out RAM');

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (1, 1, 1, 1);

--record 2--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('AMD Radeon', '1GB', 'Dedicated');

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('1TB', 'Toshiba');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel i5', '2.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Windows 7', 'SP1');

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Samsung', 'DDR3', '2GB', '2');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Desktop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES ('Monitor', '3687', 'An LG external monitor');

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (2, 2, 2, 2, 2, 2, 2, 2);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('Arrow Machines', '123, Burslem Drive', '5357290', '0876538906');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Claire Robson', '8976');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES ('Marshal', '04/004/2013', 'Normal service routine');

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (2, 2, 2, 2);

--record 3--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('Intel Graphics', '1GB', 'Onboard');

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('500GB', 'Toshiba');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel i5', '2.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Mac OS', 'OS 10.6.8');

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Samsung', 'DDR3', '2GB', '2');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Laptop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES ('89065433', 'Apple', '21 inch');

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (3, 3, 3, 3, 3, 3, 3, 3);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('JB Hifi', 'Shop 32, Perth City Mall', '7965423', '0886528075');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Marshall Matthews', '5390');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (3, 3, 3, 3);

--record 4--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('Intel Graphics', '1GB', 'Onboard');

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('500GB', 'Toshiba');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel i5', '2.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Windows 8', NULL);

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Samsung', 'DDR3', '2GB', '2');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Laptop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (4, 4, 4, 4, 4, 4, 4, 4);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('Arrow Machines', '123, Burslem Drive', '5357290', '0876538906');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Josef Atkins', '6542');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (4, 4, 4, 4); 

--record 5--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('Nvidia', '1GB', 'Dedicated');

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('320GB', 'Samsung SSD');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel i5', '2.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Ubuntu', 'Ubuntu Server');

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Kingston', 'DDR3', '4GB', '1');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Laptop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES ('Keyboard', '679830', 'Using an A4 Comfy branded keyboard');

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (5, 5, 5, 5, 5, 5, 5, 5);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('ABC Computers', '123, Burslem Drive', '67543D', '0865487524');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Josef Atkins', '6542');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (5, 5, 5, 5); 

--record 6--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('Radeon', NULL, NULL);

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('500GB', 'Samsung SSD');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel i7', '2.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Windows 7', 'SP1');

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Sandisk', 'DDR3', '2GB', '1');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Laptop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES ('Mouse', '679830', 'Using a Microsoft branded mouse');

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (6, 6, 6, 6, 6, 6, 6, 6);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('ABC Computers', '123, Burslem Drive', '67543D', '0865487524');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Harry Anderson', '9065');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (6, 6, 6, 6); 

--record 7--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('Intel Graphics', NULL, NULL);

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('500GB', 'Samsung SSD');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel i7', '2.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Windows 7', 'SP1');

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Sandisk', 'DDR3', '2GB', '1');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Laptop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES ('Mouse', '679830', 'Using a Microsoft branded mouse');

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (7, 7, 7, 7, 7, 7, 7, 7);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('ABC Computers', '123, Burslem Drive', '67543D', '0865487524');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Claire Robson', '8976');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (7, 7, 7, 7); 

--record 8--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('AMD Graphics', NULL, NULL);

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('500GB', 'Samsung SSD');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('AMD', '2.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Windows 7', 'SP1');

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Sandisk', 'DDR3', '2GB', '1');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Laptop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES ('Mouse', '679830', 'Using a Microsoft branded mouse');

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (8, 8, 8, 8, 8, 8, 8, 8);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('ABC Computers', '123, Burslem Drive', '67543D', '0865487524');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('John Wright', '2300');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (8, 8, 8, 8); 

--record 9--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('AMD Graphics', NULL, NULL);

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('128GB', 'Corsair SSD');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel', '1.3Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Windows 8', NULL);

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Kingston', 'DDR3', '2GB', '2');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Laptop');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (9, 9, 9, 9, 9, 9, 9, 9);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('JB Hifi', 'Shop 32, Perth City Mall', '7965423', '0886528075');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Jason Pitt', '8064');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (9, 9, 9, 9); 

--record 10--
INSERT INTO ims_database.gpu (gpu_brand, gpu_ram, gpu_type) 
	VALUES ('Radeon', '2GB', 'Dedicated');

INSERT INTO ims_database.hdd (hdd_capacity, hdd_brand) 
	VALUES ('512GB', 'Corsair SSD');

INSERT INTO ims_database.cpu (cpu_brand, cpu_speed) 
	VALUES ('Intel i7', '2.7Ghz');

INSERT INTO ims_database.os (os_name, os_version) 
	VALUES ('Windows 8', NULL);

INSERT INTO ims_database.ram (ram_brand, ram_type, ram_capacity, ram_count) 
	VALUES ('Samsung', 'DDR3', '4GB', '2');
--class can be one of: Server, Laptop, Desktop PC--
INSERT INTO ims_database.pc_class (pc_class_name) 
	VALUES ('Desktop PC');

INSERT INTO ims_database.peripheral (peripheral_type, peripheral_serial_no, peripheral_description) 
	VALUES ('Keyboard', '793847', 'External Microsoft keyboard');

INSERT INTO ims_database.monitor (monitor_serial_no, monitor_brand, monitor_size) 
	VALUES ('47874', 'LG', '21 inch');

INSERT INTO ims_database.pc_main (gpu_id, hdd_id, cpu_id, os_id, ram_id, monitor_id, peripheral_id, pc_class_id) 
	VALUES (10, 10, 10, 10, 10, 10, 10, 10);

INSERT INTO ims_database.vendor (vendor_name, vendor_address, vendor_invoice_number, vendor_phone) 
	VALUES ('JB Hifi', 'Shop 32, Perth City Mall', '7965423', '0886528075');

INSERT INTO ims_database.`user` (user_name, user_staff_id) 
	VALUES ('Jack Block', '8495');

INSERT INTO ims_database.maintenance (maintenance_technician, maintenance_date, maintenance_description) 
	VALUES (NULL, NULL, NULL);

INSERT INTO ims_database.asset (maintenance_id, user_id, vendor_id, pc_main_id) 
	VALUES (10, 10, 10, 10); 