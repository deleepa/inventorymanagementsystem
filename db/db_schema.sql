-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'asset'
-- 
-- ---

DROP TABLE IF EXISTS `asset`;
		
CREATE TABLE `asset` (
  `asset_id` INT(11) NOT NULL AUTO_INCREMENT,
  `maintenance_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `vendor_id` INT(11) NOT NULL,
  `pc_main_id` INT(11) NOT NULL,
  PRIMARY KEY (`asset_id`)
);

-- ---
-- Table 'gpu'
-- 
-- ---

DROP TABLE IF EXISTS `gpu`;
		
CREATE TABLE `gpu` (
  `gpu_id` INT(11) NOT NULL AUTO_INCREMENT,
  `gpu_brand` VARCHAR(50) NULL DEFAULT NULL,
  `gpu_ram` VARCHAR(20) NULL DEFAULT NULL,
  `gpu_type` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`gpu_id`)
);

-- ---
-- Table 'cpu'
-- 
-- ---

DROP TABLE IF EXISTS `cpu`;
		
CREATE TABLE `cpu` (
  `cpu_id` INT(11) NOT NULL AUTO_INCREMENT,
  `cpu_brand` VARCHAR(20) NULL DEFAULT NULL,
  `cpu_speed` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`cpu_id`)
);

-- ---
-- Table 'os'
-- 
-- ---

DROP TABLE IF EXISTS `os`;
		
CREATE TABLE `os` (
  `os_id` INT(11) NOT NULL AUTO_INCREMENT,
  `os_name` VARCHAR(20) NULL DEFAULT NULL,
  `os_version` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`os_id`)
);

-- ---
-- Table 'ram'
-- 
-- ---

DROP TABLE IF EXISTS `ram`;
		
CREATE TABLE `ram` (
  `ram_id` INT(11) NOT NULL AUTO_INCREMENT,
  `ram_brand` VARCHAR(20) NULL DEFAULT NULL,
  `ram_type` VARCHAR(20) NULL DEFAULT NULL,
  `ram_capacity` VARCHAR(20) NULL DEFAULT NULL,
  `ram_count` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`ram_id`)
);

-- ---
-- Table 'hdd'
-- 
-- ---

DROP TABLE IF EXISTS `hdd`;
		
CREATE TABLE `hdd` (
  `hdd_id` INT(11) NOT NULL AUTO_INCREMENT,
  `hdd_capacity` VARCHAR(20) NULL DEFAULT NULL,
  `hdd_brand` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`hdd_id`)
);

-- ---
-- Table 'monitor'
-- 
-- ---

DROP TABLE IF EXISTS `monitor`;
		
CREATE TABLE `monitor` (
  `monitor_id` INT(11) NOT NULL AUTO_INCREMENT,
  `monitor_serial_no` VARCHAR(50) NULL DEFAULT NULL,
  `monitor_brand` VARCHAR(20) NULL DEFAULT NULL,
  `monitor_size` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`monitor_id`)
);

-- ---
-- Table 'peripheral'
-- 
-- ---

DROP TABLE IF EXISTS `peripheral`;
		
CREATE TABLE `peripheral` (
  `peripheral_id` INT(11) NOT NULL AUTO_INCREMENT,
  `peripheral_type` VARCHAR(20) NULL DEFAULT NULL,
  `peripheral_serial_no` VARCHAR(50) NULL DEFAULT NULL,
  `peripheral_description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`peripheral_id`)
);

-- ---
-- Table 'pc_class'
-- 
-- ---

DROP TABLE IF EXISTS `pc_class`;
		
CREATE TABLE `pc_class` (
  `pc_class_id` INT(11) NOT NULL AUTO_INCREMENT,
  `pc_class_name` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`pc_class_id`)
);

-- ---
-- Table 'pc_main'
-- 
-- ---

DROP TABLE IF EXISTS `pc_main`;
		
CREATE TABLE `pc_main` (
  `pc_main_id` INT(11) NOT NULL AUTO_INCREMENT,
  `gpu_id` INT(11) NOT NULL,
  `hdd_id` INT(11) NOT NULL,
  `cpu_id` INT(11) NOT NULL,
  `os_id` INT(11) NOT NULL,
  `ram_id` INT(11) NOT NULL,
  `monitor_id` INT(11) NOT NULL,
  `peripheral_id` INT(11) NOT NULL,
  `pc_class_id` INT(11) NOT NULL,
  PRIMARY KEY (`pc_main_id`)
);

-- ---
-- Table 'vendor'
-- 
-- ---

DROP TABLE IF EXISTS `vendor`;
		
CREATE TABLE `vendor` (
  `vendor_id` INT(11) NOT NULL AUTO_INCREMENT,
  `vendor_name` VARCHAR(50) NULL DEFAULT NULL,
  `vendor_address` VARCHAR(150) NULL DEFAULT NULL,
  `vendor_invoice_number` VARCHAR(50) NULL DEFAULT NULL,
  `vendor_phone` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`vendor_id`)
);

-- ---
-- Table 'maintenance'
-- 
-- ---

DROP TABLE IF EXISTS `maintenance`;
		
CREATE TABLE `maintenance` (
  `maintenance_id` INT(11) NOT NULL AUTO_INCREMENT,
  `maintenance_technician` VARCHAR(50) NULL DEFAULT NULL,
  `maintenance_date` VARCHAR(50) NULL DEFAULT NULL,
  `maintenance_description` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`maintenance_id`)
);

-- ---
-- Table 'user'
-- 
-- ---

DROP TABLE IF EXISTS `user`;
		
CREATE TABLE `user` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(50) NOT NULL,
  `user_staff_id` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`user_id`)
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `asset` ADD FOREIGN KEY (maintenance_id) REFERENCES `maintenance` (`maintenance_id`);
ALTER TABLE `asset` ADD FOREIGN KEY (user_id) REFERENCES `user` (`user_id`);
ALTER TABLE `asset` ADD FOREIGN KEY (vendor_id) REFERENCES `vendor` (`vendor_id`);
ALTER TABLE `asset` ADD FOREIGN KEY (pc_main_id) REFERENCES `pc_main` (`pc_main_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (gpu_id) REFERENCES `gpu` (`gpu_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (hdd_id) REFERENCES `hdd` (`hdd_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (cpu_id) REFERENCES `cpu` (`cpu_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (os_id) REFERENCES `os` (`os_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (ram_id) REFERENCES `ram` (`ram_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (monitor_id) REFERENCES `monitor` (`monitor_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (peripheral_id) REFERENCES `peripheral` (`peripheral_id`);
ALTER TABLE `pc_main` ADD FOREIGN KEY (pc_class_id) REFERENCES `pc_class` (`pc_class_id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `asset` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `gpu` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `cpu` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `os` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `ram` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `hdd` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `monitor` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `peripheral` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `pc_class` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `pc_main` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `vendor` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `maintenance` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `user` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `asset` (`asset_id`,`maintenance_id_maintenance`,`user_id_user`,`vendor_id_vendor`,`pc_main_id_pc_main`) VALUES
-- ('','','','','');
-- INSERT INTO `gpu` (`gpu_id`,`gpu_brand`,`gpu_ram`,`gpu_type`) VALUES
-- ('','','','');
-- INSERT INTO `cpu` (`cpu_id`,`cpu_brand`,`cpu_speed`) VALUES
-- ('','','');
-- INSERT INTO `os` (`os_id`,`os_name`,`os_version`) VALUES
-- ('','','');
-- INSERT INTO `ram` (`ram_id`,`ram_brand`,`ram_type`,`ram_capacity`,`ram_count`) VALUES
-- ('','','','','');
-- INSERT INTO `hdd` (`hdd_id`,`hdd_capacity`,`hdd_brand`) VALUES
-- ('','','');
-- INSERT INTO `monitor` (`monitor_id`,`monitor_serial_no`,`monitor_brand`,`monitor_size`) VALUES
-- ('','','','');
-- INSERT INTO `peripheral` (`peripheral_id`,`peripheral_type`,`peripheral_serial_no`,`peripheral_description`) VALUES
-- ('','','','');
-- INSERT INTO `pc_class` (`pc_class_id`,`pc_class_name`) VALUES
-- ('','');
-- INSERT INTO `pc_main` (`pc_main_id`,`gpu_id_gpu`,`hdd_id_hdd`,`cpu_id_cpu`,`os_id_os`,`ram_id_ram`,`monitor_id_monitor`,`peripheral_id_peripheral`,`pc_class_id_pc_class`) VALUES
-- ('','','','','','','','','');
-- INSERT INTO `vendor` (`vendor_id`,`vendor_name`,`vendor_address`,`vendor_invoice_number`,`vendor_phone`) VALUES
-- ('','','','','');
-- INSERT INTO `maintenance` (`maintenance_id`,`maintenance_technician`,`maintenance_date`,`maintenance_description`) VALUES
-- ('','','','');
-- INSERT INTO `user` (`user_id`,`user_name`,`user_staff_id`) VALUES
-- ('','','');